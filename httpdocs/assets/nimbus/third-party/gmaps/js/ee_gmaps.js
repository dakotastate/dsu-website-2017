/*!
 * EE_GMaps.js
 * http://reinos.nl
 *
 * Copyright 2012, Rein de Vries.
 */
;var EE_GMAPS = EE_GMAPS || {}; 

(function ($) {
	
	//get latlong based on address
	EE_GMAPS.setGeocoding = function (options) {
		//merge default settings with given settings
		var options = $.extend( {
			'selector'							: '',
			'map_type' 							: '',
			'address' 							: '',
			'zoom'	 							: '',
			'width' 							: '',
			'height' 							: '',
			'loader' 							: '',
			'marker' 							: true,
			'marker_html' 						: '',
			'marker_icon' 						: '',
			'custom_marker_vertical_align' 		: '',
			'custom_marker_horizontal_align' 	: '',
			'custom_marker_html'				: '',
			'static' 							: true,
			'randomNr'							: Math.floor(Math.random()*11),
			'zoom_control'						: true,
			'pan_control' 						: true,
			'map_type_control' 					: true,
			'scale_control' 					: true,
			'street_view_control' 				: true
		}, options);
		
		//is this a static map?
		if(options.static) {
			EE_GMAPS.setStaticMap({
				'selector'	: options.selector,
				'address' 	: options.address,
				'map_type' 	: options.map_type,
				'width' 	: options.width,
				'height' 	: options.height,
				'zoom'	 	: options.zoom,
				'marker'	: options.marker
			});
			return true;
		}

		//get address
		EE_GMAPS.latLongAddress(options.address, function(latlng, latlngObj){
			latlng = latlng[0];
			var map = new GMaps({
				div: options.selector,
				lat: latlng.lat(),
				lng: latlng.lng(),
				width : options.width,
				height : options.height,
				zoom : options.zoom,
				mapType : options.map_type,
				zoomControl : options.zoom_control,
				panControl : options.pan_control,
				mapTypeControl : options.map_type_control,
				scaleControl : options.scale_control,
				streetViewControl : options.street_view_control
			});
			
			//place marker
			if(options.marker) {

				//custom marker
				if(options.custom_marker_html) {
					map.drawOverlay({
						lat: map.getCenter().lat(),
						lng: map.getCenter().lng(),
						content: options.custom_marker_html,
						verticalAlign: options.custom_marker_vertical_align,
						horizontalAlign: options.custom_marker_vertical_align
					});
				} else {
					map.addMarker({
						lat: latlng.lat(),
						lng: latlng.lng(),
						title : latlngObj[0].formatted_address,
						icon : options.marker_icon,
						infoWindow: {
							content: options.marker_html
						}
					});	
				}
			}
		});	
	};

	//get latlong based on address
	EE_GMAPS.setGeolocation = function (options) {
		//merge default settings with given settings
		var options = $.extend( {
			'selector'				: '',
			'width' 				: '',
			'map_type' 				: '',
			'height' 				: '',
			'marker'	 			: true,
			'marker_icon' 			: '',
			'zoom_control'			: true,
			'pan_control' 			: true,
			'map_type_control' 		: true,
			'scale_control' 		: true,
			'street_view_control' 	: true
		}, options);
		
		map = new GMaps({
			div: options.selector,
			zoom : 1,
			lat: -12.043333,
       		lng: -77.028333,
			width : options.width,
			height : options.height,
			mapType : options.map_type,
			zoomControl : options.zoom_control,
			panControl : options.pan_control,
			mapTypeControl : options.map_type_control,
			scaleControl : options.scale_control,
			streetViewControl : options.street_view_control
		});

		GMaps.geolocate({
			success: function(position) {
				map.setCenter(position.coords.latitude, position.coords.longitude);
				map.setZoom(10);
				
				//place marker 
				if(options.marker) {
					map.addMarker({
						lat: position.coords.latitude,
						lng:  position.coords.longitude,
						icon : options.marker_icon
					});	
				}
			},
			error: function(error) {
				alert('Geolocation failed: '+error.message);
			},
			not_supported: function() {
				alert("Your browser does not support geolocation");
			},
			always: function() {
				//alert("Done!");
			}
		});
	};

	//get latlong based on address
	EE_GMAPS.setPolygon = function (options) {
		//merge default settings with given settings
		var options = $.extend( {
			'selector'				: '',
			'address' 				: [],
			'map_type' 				: '',
			'width' 				: '',
			'height' 				: '',
			'stroke_color'	 		: '',
			'stroke_opacity'		: '',
			'stroke_weight'	 		: '',
			'fill_color'	 		: '',
			'fill_opacity'	 		: '',
			'marker'				: true,
			'marker_icon'			: '',
			'static'				: true,
			'zoom_control'			: true,
			'pan_control' 			: true,
			'map_type_control' 		: true,
			'scale_control' 		: true,
			'street_view_control' 	: true
		}, options);
		
		//is this a static map?
		if(options.static) {
			EE_GMAPS.setStaticMap({
				'selector'			: options.selector,
				'address' 			: options.address,
				'map_type' 			: options.map_type,
				'width' 			: options.width,
				'height' 			: options.height,
				'zoom'	 			: options.zoom,
				'marker'			: options.marker,
				'polygon'			: true,
				'stroke_color'	 	: options.stroke_color,
				'stroke_opacity'	: options.stroke_opacity,
				'stroke_weight'	 	: options.stroke_weight,
				'fill_color'	 	: options.fill_color,
				'fill_opacity'	 	: options.fill_opacity
			});
			return true;
		}

		var map = new GMaps({
			div: options.selector,
			width : options.width,
			height : options.height,
			zoom : options.zoom,
			mapType : options.map_type,
			zoomControl : options.zoom_control,
			panControl : options.pan_control,
			mapTypeControl : options.map_type_control,
			scaleControl : options.scale_control,
			streetViewControl : options.street_view_control
		});

		//get address
		EE_GMAPS.latLongAddress(options.address, function(latlng, latlngObj){
			var _polygon_addresses = [];

			$.each(latlng, function(k,v){
				_polygon_addresses.push([v.lat(), v.lng()]);
			});

			polygon = map.drawPolygon({
				paths: _polygon_addresses, // pre-defined polygon shape
				strokeColor: options.stroke_color,
				strokeOpacity: options.stroke_opacity,
				strokeWeight: options.stroke_weight,
				fillColor: options.fill_color,
				fillOpacity: options.fill_opacity
			});

			//fit the map
			map.fitBounds(latlng);

			//place marker
			if(options.marker) {
				$.each(latlng, function(k,v){
					map.addMarker({
						lat: v.lat(),
						lng: v.lng(),
						title : latlngObj[k].formatted_address,
						icon : options.marker_icon
					});	
				});	
			}
		});
	};


	//get latlong based on address
	EE_GMAPS.setCircle = function (options) {
		//merge default settings with given settings
		var options = $.extend( {
			'selector'				: '',
			'address' 				: [],
			'map_type' 				: '',
			'width' 				: '',
			'height' 				: '',
			'marker'				: true,
			'marker_icon'			: '',
			'zoom'					: '',
			'fit_circle'			: '',
			'stroke_color'	 		: options.stroke_color,
			'stroke_opacity'		: options.stroke_opacity,
			'stroke_weight'	 		: options.stroke_weight,
			'fill_color'	 		: options.fill_color,
			'fill_opacity'	 		: options.fill_opacity,
			'radius'	 			: options.radius,
			'zoom_control'			: true,
			'pan_control' 			: true,
			'map_type_control' 		: true,
			'scale_control' 		: true,
			'street_view_control' 	: true
		}, options);
		
		
		//get address
		EE_GMAPS.latLongAddress(options.address, function(latlng, latlngObj){
			latlng = latlng[0];
			
			var map = new GMaps({
				div: options.selector,
				lat: latlng.lat(),
				lng: latlng.lng(),
				width : options.width,
				height : options.height,
				zoom : options.zoom,
				mapType : options.map_type,
				zoomControl : options.zoom_control,
				panControl : options.pan_control,
				mapTypeControl : options.map_type_control,
				scaleControl : options.scale_control,
				streetViewControl : options.street_view_control
			});

			var circle = map.drawCircle({
				strokeColor: options.stroke_color,
				strokeOpacity: options.stroke_opacity,
				strokeWeight: options.stroke_weight,
				fillColor: options.fill_color,
				fillOpacity: options.fill_opacity,
				radius: options.radius,
				lat: latlng.lat(),
				lng: latlng.lng()
			});

			//fit the map
			if(options.fit_circle){
				map.map.fitBounds(circle.getBounds());
			}

			//place marker
			if(options.marker) {
				map.addMarker({
					lat: latlng.lat(),
					lng: latlng.lng(),
					title : latlngObj[0].formatted_address,
					icon : options.marker_icon
				});	
			}
		});
	};
	
	
	//get latlong based on address
	EE_GMAPS.setStaticMap = function (options) {
		//merge default settings with given settings
		var options = $.extend( {
			'selector'			: '',
			'address' 			: '',
			'map_type' 			: '',
			'width' 			: '',
			'height' 			: '',
			'zoom'	 			: '',
			'marker'			: true,
			'polygon'			: false,
			'stroke_color'	 	: '',
			'stroke_opacity'	: '',
			'stroke_weight'	 	: '',
			'fill_color'	 	: '',
			'fill_opacity'	 	: '',
		}, options);

		//get address
		EE_GMAPS.latLongAddress(options.address, function(latlng){
			if(options.polygon) {

				var _polygon_addresses = [];
				var _markers = [];

				$.each(latlng, function(k,v){
					_polygon_addresses.push([v.lat(), v.lng()]);

					if(options.marker) {
						_markers.push({lat:v.lat(), lng:v.lng()});
					}
				});

				var center = EE_GMAPS.getCenterLatLng(latlng);

				//create map
				var url = GMaps.staticMapURL({
					size: [options.width, options.height],
					lat: center[0],
					lng: center[1],
					zoom : EE_GMAPS.getZoom(options.width, latlng),
					maptype : options.map_type,
					polyline: {
						path: _polygon_addresses,
						strokeColor: options.stroke_color,
						strokeOpacity: options.stroke_opacity,
						strokeWeight: options.stroke_weight,
						fillColor: options.fill_color
					},
					markers: _markers
				});

			//geocoding
			} else {
				latlng = latlng[0];

				//create map
				var url = GMaps.staticMapURL({
					size: [options.width, options.height],
					lat: latlng.lat(),
					lng: latlng.lng(),
					zoom : options.zoom,
					maptype : options.map_type,
					markers: [{
						lat: latlng.lat(), 
						lng: latlng.lng()
					}]
				});
			}

			//place the image
			$(options.selector).html('<img src="'+url+'" alt="Gmaps map from '+options.address+'" title="Gmaps map from '+options.address+'" width="'+options.width+'" height="'+options.height+'" />');
			
		});
	};
	
	
	//Route address
	EE_GMAPS.setRoute = function (options) {	
		
		//merge default settings with given settings
		var options = $.extend( {
			'selector'       		: '',
			'from_address' 			: '',
			'to_address' 			: '',
			'stopsaddresses' 		: [],
			'map_type' 				: '',
			'travel_mode'			: "driving",
			'stroke_color'			: "#131540",
			'stroke_opacity'		: 0.6,
			'stroke_weight'			: 6,
			'markers'				: true,
			'marker_icon'			: '',
			'width' 				: '',
			'height' 				: '',
			'zoom_control'			: true,
			'pan_control' 			: true,
			'map_type_control' 		: true,
			'scale_control' 		: true,
			'street_view_control' 	: true
		}, options);

		//set map 
		map = new GMaps({
			div: options.selector,
			lat: -12.043333,
			lng: -77.028333,
			width : options.width,
			height : options.height,
			mapType : options.map_type,
			zoomControl : options.zoom_control,
			panControl : options.pan_control,
			mapTypeControl : options.map_type_control,
			scaleControl : options.scale_control,
			streetViewControl : options.street_view_control
		});
		
		//concat all points
		var points = [options.from_address, options.to_address].concat(options.stopsaddresses);
		
		//get address
		EE_GMAPS.latLongAddress(points, function(latlng, latlngObj){

			//cache the locations for pinPoints purpose
			var markers = latlng.clone();

			//set from and to
			from = latlng[0];
			to = latlng[1];

			//unset from and to
			latlng.splice(0,2)
			
			//create waypoints
			var waypoints = EE_GMAPS.createWaypoints(latlng);

			//draw the route
			map.drawRoute({
				origin: [from.lat(), from.lng()],
				destination: [to.lat(), to.lng()],
				waypoints : waypoints,
				travelMode: options.travel_mode,
				strokeColor: options.stroke_color,
				strokeOpacity: options.stroke_opacity,
				strokeWeight: options.stroke_weight
			});
			
			//fit the map
			map.fitBounds(markers);
			
			//place the markers
			if(options.markers) {
				$.each(markers, function(k,v){
					map.addMarker({
					  lat: v.lat(),
					  lng: v.lng(),
					  title : latlngObj[k].formatted_address,
					  icon : options.marker_icon
					});
				});
			}
		});	
	}
	
	
	//get latlong based on address
	EE_GMAPS.latLongAddress = function (addresses, callback) {
		 
		var latLongAdresses = new Array();
		var latLongObject = new Array();

		$.each(addresses,function(key,val){
			GMaps.geocode({
				address: val,
				callback: function(results, status){
					if(status=="OK"){
						var latlng = results[0].geometry.location;
						latLongAdresses[key] = latlng;
						latLongObject[key] = results[0];
						if(key == (addresses.length - 1)) {
							if (callback && typeof(callback) === "function") {
								//settimeout because the load error
								setTimeout(function(){callback(latLongAdresses, latLongObject);}, 100);
							}
						}
					}
				}
			});
		});
	};
	
	//retrun good waypoints array based on some latlong positions
	EE_GMAPS.getCenterLatLng = function (latlong) { 
		var lat = [];
		var lng = [];

		$.each(latlong,function(key,val){
			lat.push(val.lat());	
			lng.push(val.lng());	
		});

		//set the center x and y points
		var center_lat = lat.min() + ((lat.max() - lat.min()) / 2);
		var center_lng = lng.min() + ((lng.max() - lng.min()) / 2);

		return [center_lat, center_lng];
	};


	//calculate the zoom
	EE_GMAPS.getZoom = function (map_width, latlong) { 
		
		var lat = [];
		var lng = [];

		$.each(latlong,function(key,val){
			lat.push(val.lat());	
			lng.push(val.lng());	
		});

		//calculate the distance
		var dist = (6371 * Math.acos(Math.sin(lat.min() / 57.2958) * Math.sin(lat.max() / 57.2958) + 
		            (Math.cos(lat.min() / 57.2958) * Math.cos(lat.max() / 57.2958) * Math.cos((lng.max() / 57.2958) - (lng.min() / 57.2958)))));

		//calculate the zoom
		var zoom = Math.floor(8 - Math.log(1.6446 * dist / Math.sqrt(2 * (map_width * map_width))) / Math.log (2)) - 1;
	
		return zoom;
	};


	//retrun good waypoints array based on some latlong positions
	EE_GMAPS.createWaypoints = function (waypoints) { 
		var points = [];
		$.each(waypoints,function(key,val){
			points.push({
				location : val,
				stopover: false
			});	
		});
		return points;
	};
	
	
	//clone array
	Array.prototype.clone = function() { return this.slice(0); }

	Array.prototype.max = function() {
	  return Math.max.apply(null, this)
	}

	Array.prototype.min = function() {
	  return Math.min.apply(null, this)
	}

}(jQuery));


 

	
	
	