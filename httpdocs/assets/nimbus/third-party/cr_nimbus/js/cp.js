function getQueryParams()
{
	var searchString = window.location.search.substring(1),
		params = searchString.split("&"),
		hash = {};

	for (var i = 0; i < params.length; i++) {
		var val = params[i].split("=");
		hash[unescape(val[0])] = unescape(val[1]);
	}

	return hash;
}

(function( $ ){
	var methods = {
		group: function() {
			var numEls = this.length;
			return this.each(function(i){
				var $this = $(this);
				$this.css('width',String(Math.floor(100 / numEls)) + '%');
				if ( i === 0 ) {
					$this.css('clear','left');
				}
			});
		},
		height: function( h ) {
			return this.each(function(){
				var $this = $(this);
				$this.css('height',h + 'px');
			});
		},
		title: function( content ) {
			return this.each(function(){
				var $this = $(this),
					$cImg = $('label.hide_field span img',$(this)).clone(),
					$cEm = $('label.hide_field span em',$(this)).clone();
				if ( $cEm.length ) {
					$('label.hide_field span',$this).html($cImg[0].outerHTML+$cEm[0].outerHTML+' '+content);
				} else {
					$('label.hide_field span',$this).html($cImg[0].outerHTML+' '+content);
				}
			});
		},
		instructions: function( content, append ) {
			return this.each(function(){
				var $this = $(this);
				if ( $('label.hide_field div.instruction_text',$this).length ) {
					$('label.hide_field div.instruction_text',$this).html('<p>'+content+'</p>');
				} else if ( $('div div.instruction_text',$this).length ) {
					$('div div.instruction_text',$this).html('<p>'+content+'</p>');
				} else {
					$('label.hide_field',$this).after('<div class="instruction_text"><p>'+content+'</p></div>');
				}
			});
		}
	};
	$.fn.modField = function( method ) {
		if ( methods[method] ) {
			return methods[method].apply( this, Array.prototype.slice.call( arguments, 1 ));
		} else if ( typeof method === 'object' || ! method ) {
			return methods.init.apply( this, arguments );
		} else {
			$.error( 'Method ' +  method + ' does not exist on jQuery.tooltip' );
		}
	};
})(jQuery);

$(function(){

	var q = getQueryParams();



	// Global Publish & Edit Form Changes
	if ( $('#publishForm').length ) {



		// Sync Structure URL Title & Native URL Title
		if ( $('#hold_field_structure__uri').length && $('#hold_field_url_title').length ) {
			$('#hold_field_structure__uri').on('keyup keypress blur change','#structure__uri',function(){
				$('#hold_field_url_title #url_title').val($(this).val());
			});
		}

		// Group Title, URL Title, and Structure URL
		$('#hold_field_title,#hold_field_url_title,#hold_field_structure__uri').modField('group');

	}



	// Channel-specific Publish & Edit Form Changes
	// ============================================
	// Change XX to match the intended channel_id
	//
	if ( $('#publishForm').length && $('#publishForm input[name="channel_id"]').val() == 'XX' ) {



		// Example of Grouping Fields
		// ==========================
		// Compressing Titles
		//
		// $('#hold_field_title,#hold_field_structure__uri,#hold_field_url_title').modField('group');



		// Example of Constraining Field Widths
		// ====================================
		// Constraining Titles
		//
		// $('#hold_field_title,#hold_field_structure__uri,#hold_field_url_title').css('width','33%');



		// Date & Time Field Hacks
		// =======================
		// This ... is rudimentary and it's not pretty, but it's functional.
		//
		//  - Replace field_id_XX below with the start_time field_id.
		//  - Replace field_id_YY below with the end_time field_id.
		//
		// $('#hold_field_entry_date').removeClass('hidden');
		// $('#sub_hold_field_entry_date input#entry_date,#sub_hold_field_entry_date select,#sub_hold_field_expiration_date input#expiration_date').// addClass('hidden');
		// var preD = [];
		// var startDateValue = $('#sub_hold_field_entry_date input#entry_date').val();
		// var endDateValue = $('#sub_hold_field_expiration_date input#expiration_date').val();
		//
		// preD[0] = startDateValue.split(' ').shift().split('-');
		// preD[1] = $('#sub_hold_field_expiration_date input#expiration_date').val().split(' ').shift().split('-');
		// var mos = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
		// var y = [],m = [],d = [],t = '',s = [];
		//
		// if (startDateValue === '') {
		// 	var dd = new Date();
		// 	preD[0][0] = dd.getFullYear();
		// 	preD[0][1] = dd.getMonth() + 1;
		// 	preD[0][2] = dd.getDate();
		// }
		// if (endDateValue === '') {
		// 	preD[1] = preD[0];
		// }
		//
		// for (b=0;b<2;b++) {
		//
		// 	// Set Years
		// 	t = '';
		// 	for (i=2010;i<2031;i++) {
		// 		s[b] = (preD[b][0] == i)? ' selected="selected"': '';
		// 		t = t+'<option value="'+i+'"'+s[b]+'>'+i+'</option>';
		// 	}
		// 	y[b] = '<select name="y" id="dsy'+b+'">'+t+'</select>';
		//
		// 	// Set Months
		// 	t = '';
		// 	for (i=1;i<13;i++) {
		// 		s[b] = (preD[b][1] == i)? ' selected="selected"': '';
		// 		i = (i < 10)? '0'+i: i;
		// 		t = t+'<option value="'+i+'"'+s[b]+'>'+mos[i-1]+'</option>';
		// 	}
		// 	m[b] = '<select name="m" id="dsm'+b+'">'+t+'</select>';
		//
		// 	// Set Days
		// 	t = '';
		// 	for (i=1;i<32;i++) {
		// 		s[b] = (preD[b][2] == i)? ' selected="selected"': '';
		// 		i = (i < 10)? '0'+i: i;
		// 		t = t+'<option value="'+i+'"'+s[b]+'>'+i+'</option>';
		// 	}
		// 	d[b] = '<select name="d" id="dsd'+b+'">'+t+'</select>';
		// }
		//
		// $('#sub_hold_field_entry_date .holder').prepend(m[0]+'&nbsp; '+d[0]+'&nbsp; '+y[0]);
		// $('#sub_hold_field_expiration_date .holder').prepend(m[1]+'&nbsp; '+d[1]+'&nbsp; '+y[1]);
		//
		// var h,i;
		// y = '';
		// m = '';
		// d = '';
		// s = '';
		//
		// $('#hold_field_entry_date select,#hold_field_id_XX select').change(function() {
		// 	y = $('#hold_field_entry_date #dsy0 option:selected').val();
		// 	m = $('#hold_field_entry_date #dsm0 option:selected').val();
		// 	d = $('#hold_field_entry_date #dsd0 option:selected').val();
		// 	h = $('#hold_field_id_XX select:eq(0) option:selected').val();
		// 	i = $('#hold_field_id_XX select:eq(1) option:selected').val();
		// 	s = $('#hold_field_id_XX select:eq(2) option:selected').val();
		// 	$('#sub_hold_field_entry_date input#entry_date').val(y+'-'+m+'-'+d+' '+h+':'+i+':00 '+s);
		// });
		// $('#hold_field_expiration_date select,#hold_field_id_YY select').change(function() {
		// 	y = $('#hold_field_expiration_date #dsy1 option:selected').val();
		// 	m = $('#hold_field_expiration_date #dsm1 option:selected').val();
		// 	d = $('#hold_field_expiration_date #dsd1 option:selected').val();
		// 	h = $('#hold_field_id_YY select:eq(0) option:selected').val();
		// 	i = $('#hold_field_id_YY select:eq(1) option:selected').val();
		// 	s = $('#hold_field_id_YY select:eq(2) option:selected').val();
		// 	$('#sub_hold_field_expiration_date input#expiration_date').val(y+'-'+m+'-'+d+' '+h+':'+i+':00 '+s);
		// });



		// Example Title Modifications
		// ===========================
		// Modify Event Title Fields
		//
		// $('#hold_field_title').modField('title','Event URL Title');
		// $('#hold_field_structure__uri').modField('title','Event URL Title');
		// $('#hold_field_url_title').modField('title','Internal URL Title');



		// Example Description / Instruction Modifications
		// ===============================================
		// Modify Event Title Descriptions
		//
		// $('#hold_field_title').modField('instructions','<em>Instructions:</em> The title of the blog post.');
		// $('#hold_field_structure__uri').modField('instructions','<em>Instructions:</em> The URL-friendly event title. Alphanumeric characters only. Use dashes in place of spaces.');
		// $('#hold_field_url_title').modField('instructions','<em>Instructions:</em> This must match the Event URL Title.');



	}


	// Events
	if ( $('#publishForm').length && $('#publishForm input[name="channel_id"]').val() == '3' ) {

	}



	// Optional Structure-specific Modifications
	if ( $('#structure-ui').length ) {



		// Rename Structure to "Manage Content" (or somesuch)
		// $('#mainContent .contents .heading. .edit').text('Manage Content');



		// Add page descriptions to the "Add Page" list. (Replace XX w/ channel_id)
		// $('#add-dialog ul li a[href*="channel_id=XX"]')
		//	.append('<span class="desc">These are standard content pages. They may contain lists of rooms and specials.</span>')
		//	.addClass('standard-page');



		// Show Assets below structured pages
		// if ( $('#assets').length ) {
		// 	$('#tree-switcher').addClass('hide-alt');
		// 	$('#assets').removeClass('hide-alt').addClass('asset-ui');
		// 	$('#assets li').each(function(){
		// 		var $t = $(this);
		//
		// 		// Rearrange Asssets Bar
		// 		$('.listing-title',$t).insertAfter($('.page-listing',$t));
		//
		// 		// Theme Assets Bar
		// 		$t.addClass('listing-type-' + $('.listing-title',$t).text().toLowerCase().replace(' ','-'));
		// 	});
		// }



		// Remove Add Child button from specific pages. (Replace XX w/ page_id)
		// $('#structure-ui #page-XX.page-item .page-controls span:eq(0)').remove();



	}



	// Add Icon to Developer Tab
	if ( $('#navigationTabs #developer_acc').length ) {
		$('#navigationTabs #developer_acc').addClass('has-icon');
		$('#navigationTabs #developer_acc > a').prepend('<img src="/assets/nimbus/third-party/cr_nimbus/images/icons/gear.png" alt="Developer" class="icon" /> ');
	}



	// Add Member Group Class to Body
	if ( $('#group-id').length ) {
		$('body').addClass($('#group-id').attr('class'));
	}



	// Show page content after we've done our modifications
	$('#mainContent .contents,#breadCrumb ol').css('opacity','1');



});
