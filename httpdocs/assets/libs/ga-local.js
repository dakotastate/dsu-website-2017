// GA Local Storage
// ----------------
// (c) Click Rain, 2014; Mark Drzycimski <mark@clickrain.com>;
// Thanks to: CaldasGSM (http://stackoverflow.com/questions/19189785)



/* ------------------------------- *\
*   Define Cookie Get/Set/Destroy.  *
\* ------------------------------- */

// Get a Cookie
document.getCookie = function(sName)
{
	sName = sName.toLowerCase();
	var oCrumbles = document.cookie.split(';');
	for(var i=0; i<oCrumbles.length;i++) {
		var oPair= oCrumbles[i].split('=');
		var sKey = decodeURIComponent(oPair[0].trim().toLowerCase());
		var sValue = oPair.length>1?oPair[1]:'';

		if(sKey == sName) return decodeURIComponent(sValue);
	}
	return '';
};

// Set a Cookie
document.setCookie = function(sName,sValue)
{
	var oDate = new Date();
	oDate.setYear(oDate.getFullYear()+1);
	var sCookie = encodeURIComponent(sName) + '=' + encodeURIComponent(sValue) + ';expires=' + oDate.toGMTString() + ';path=/';
	document.cookie= sCookie;
};

// Eat a Cookie
document.clearCookie = function(sName)
{
	setCookie(sName,'');
};



/* --------------------------- *\
*   Define QueryString Utility  *
\* --------------------------- */

function crGetQueryString() {
	var qs      = window.location.search.substring(1),
		queries = qs.split('&'),
		qLength = queries.length,
		params  = {},
		temp;

	for ( var i = 0; i < qLength; i++ ) {
		temp = queries[i].split('=');
		params[temp[0]] = temp[1];
	}

	return params;
}



/* -------------------- *\
*   And now, we begin.   *
\* -------------------- */

function crGaGetTracker(i) {
	i = typeof i !== 'undefined'? i: 0;
	return ga.getAll()[i];
}

function crGaGetData(t) {
	t = typeof t !== 'undefined'? t: crGaGetTracker();

	var utmMetrics = ['referrer','screenResolution','viewportSize','encoding','screenColors','language','javaEnabled','flashVersion','location','hostname','page','title','screenName','linkid'],
		numMetrics = utmMetrics.length,
		gaData     = {};

	for(var i = 0; i < numMetrics; i++) {
		gaData[utmMetrics[i]] = t.get(utmMetrics[i]);
	}

	return gaData;
}

function crGetCrtData(gaData) {
	gaData = typeof gaData !== 'undefined'? gaData: {};

	var crtMetrics = ['crt_campaign','crt_source','crt_medium','crt_keyword','crt_creative','crt_adposition','crt_device'],
		numMetrics = crtMetrics.length,
		queryArgs  = crGetQueryString();

	for(var i = 0; i < numMetrics; i++) {
		gaData[crtMetrics[i]] = queryArgs[crtMetrics[i]];
	}
}

function crGa(i) {
	i = typeof i !== 'undefined'? i: 0;

	var gaTrk  = crGaGetTracker(i),
		gaData = crGaGetData(gaTrk),
		pData  = document.getCookie('cr_ga_data'),
		npData = {},
		opData;

	// Load CRT data
	crGetCrtData(gaData);

	// Set Transient Cookie (cr_ga_tdata);
	document.cookie= 'cr_ga_tdata=' + encodeURIComponent(JSON.stringify(gaData)) + ';path=/';

	// Set/Update Persistent Cookie (cr_ga_data);
	try {
		opData = JSON.parse(pData);

		for (var p in gaData) {
			switch (p) {
				case 'crt_campaign':
				case 'crt_source':
				case 'crt_medium':
				case 'crt_keyword':
				case 'crt_creative':
				case 'crt_adposition':
					// Retain Persistent Metrics (if undefined)
					if ( typeof gaData[p] !== 'undefined' || typeof opData[p] === 'undefined' ) {
						npData[p] = gaData[p];
					} else {
						npData[p] = opData[p];
					}
				break;

				default:
					npData[p] = gaData[p];
				break;
			}
		}
		document.setCookie('cr_ga_data',JSON.stringify(npData));
	} catch(e) {
		// Create Persistent Cookie on Error
		document.setCookie('cr_ga_data',JSON.stringify(gaData));

		// We hide the error for production use
		// console.error('Parsing Error:', e);
	}
}