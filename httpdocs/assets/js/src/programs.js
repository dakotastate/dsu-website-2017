jQuery(function($) {
	$('#programswrapper').each(function() {
		var container = $(this);

		var sections = $('.programssection');
		var programs = sections.find('.programssection-program');
		preparePrograms(programs, sections);

		var noresults = $('<p class="noresults">No results for <span></span></p>');
		var noresultstext = noresults.find('span');
		$('.programs').append(noresults);

		container.clientsideSearch({
			inputSelector: 'input',
			sectionsSelector: '.programssection',
			itemsSelector: '.programssection-program',
			indexAttribute: 'data-programsearchcontent',
			noResultsSelector: '.noresults span'
		});
	});

	function preparePrograms(programs, sections) {
		programs.each(function() {
			var program = $(this);
			var header = program.find('header').find('h1, h2, h3, h4, h5, h6, button');

			var index = header.text().toLowerCase();

			var keywords = program.attr('data-keywords');
			if (keywords) {
				index += ', ' + keywords.toLowerCase();
			}

			program.attr('data-programsearchcontent', index);
			program.attr('data-ismatch', 'true');
		});
		sections.each(function() {
			$(this).attr('data-containsmatch', 'true');
		});
	}
});
