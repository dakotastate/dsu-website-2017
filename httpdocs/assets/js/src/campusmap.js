jQuery(function($) {
	$('#campusmap-canvas').each(function() {
		var canvas = $(this);

		var styles = [
			{
				"elementType": "geometry.fill",
				"featureType": "poi.school",
				"stylers": [
					{
						"visibility": "on"
					},
					{
						"color": "#fdf9df"
					}
				]
			},
			{
				"elementType": "geometry.fill",
				"featureType": "landscape.man_made",
				"stylers": [
					{
						"visibility": "on"
					},
					{
						"color": "#a0a0a0"
					}
				]
			},
			{
				"elementType": "geometry.fill",
				"featureType": "road.local",
				"stylers": [
					{
						"color": "#b8c128"
					}
				]
			},
			{
				"elementType": "labels.text.stroke",
				"featureType": "road",
				"stylers": [
					{
						"visibility": "on"
					},
					{
						"weight": 4.2
					},
					{
						"color": "#fffada"
					}
				]
			},
			{
				"elementType": "labels.text.fill",
				"featureType": "road.local",
				"stylers": [
					{
						"visibility": "on"
					},
					{
						"weight": 2.6
					},
					{
						"color": "#000000"
					}
				]
			},
			{
				"elementType": "labels.text",
				"stylers": [
					{
						"visibility": "off"
					}
				]
			},
			{
				"elementType": "labels.text",
				"featureType": "road",
				"stylers": [
					{
						"visibility": "on"
					}
				]
			}
		];

		var mapOptions = {
			center: new google.maps.LatLng(44.0123093, -97.1120205),
			zoom: 18,
			mapTypeId: google.maps.MapTypeId.HYBRID,
			styles: styles
		};
		var map = new google.maps.Map(canvas.get(0), mapOptions);

		var image = {
			url: '/assets/images/map-markers/1.png',
			// This marker is 20 pixels wide by 32 pixels tall.
			size: new google.maps.Size(76, 71),
			// The origin for this image is 0,0.
			origin: new google.maps.Point(0,0),
			// The anchor for this image is the base of the flagpole at 0,32.
			anchor: new google.maps.Point(21, 70)
		};

		var infowindow = new google.maps.InfoWindow({
			content: "Hey."
		});
		infowindow._dsu_ = {
			currentMarker: null,
			resetCurrentMarker: function() {
				if (this.currentMarker) {
					var oldMarker = this.currentMarker;
					oldMarker.setIcon(oldMarker._dsu_.image);
					oldMarker._dsu_.point.removeClass('active');
					this.currentMarker = null;
				}
			},
			setCurrentMarker: function(marker) {
				this.resetCurrentMarker();
				if (marker) {
					this.currentMarker = marker;
					marker.setIcon(marker._dsu_.activeImage);
					marker._dsu_.point.addClass('active');
				}
			}
		};

		google.maps.event.addListener(infowindow, 'closeclick', function() {
			infowindow._dsu_.resetCurrentMarker();
		});

		var allPositions = [];
		var bounds = new google.maps.LatLngBounds();

		$('.points .point').each(function(i) {
			var point = $(this);
			var icon = point.attr('data-icon');
			var title = point.find('.title').text();
			var markerUrl = '/assets/images/mapmarkers.png';

			var latlngParts = point.attr('data-latlng').split(',');
			var latlng = {
				lat: parseFloat(latlngParts[0]),
				lng: parseFloat(latlngParts[1]),
			};

			var spriteX = 0;

			if (/\d+/.test(icon)) {
				var iconNumber = parseInt(icon, 10);
				// On the rare chance that they end up putting more than 31
				// points of interest in, then we don't have any more sprites
				// to show. So, show them the empty sprite at the beginning.
				// It's the best I can do. They have 16 points of interest
				// defined at launch time, so I'm just hoping they don't
				// double that before this site becomes outdated.
				if (iconNumber < 31) {
					spriteX = 44 * iconNumber;
				}
			}
			else if (icon === "star") {
				spriteX = 44 * 32;
			}
			else if (icon === "parking") {
				spriteX = 44 * 33;
			}

			var image = {
				url: markerUrl,
				size: new google.maps.Size(44, 58),
				origin: new google.maps.Point(spriteX, 0)
			};

			var activeImage = {
				url: image.url,
				size: image.size,
				origin: new google.maps.Point(spriteX, 58)
			};

			var marker = new google.maps.Marker({
				position: latlng,
				map: map,
				icon: image,
				title: title
			});

			marker._dsu_ = {
				point: point,
				image: image,
				activeImage: activeImage
			};

			google.maps.event.addListener(marker, 'click', function() {
				infowindow.close();
				infowindow._dsu_.setCurrentMarker(marker);
				infowindow.setContent(point.find('.pincontent').clone().get(0));
				infowindow.open(map, marker);
			});

			point.data('marker', marker);
			point.addClass('clickable');

			bounds.extend(new google.maps.LatLng(latlng.lat, latlng.lng));
		});

		$('.points').on('click', '.point', function(e) {
			var point = $(this);
			var marker = point.data('marker');
			infowindow.close();
			infowindow._dsu_.setCurrentMarker(marker);
			infowindow.setContent(point.find('.pincontent').clone().get(0));
			infowindow.open(map, marker);
		});

		map.fitBounds(bounds);
	});
});
