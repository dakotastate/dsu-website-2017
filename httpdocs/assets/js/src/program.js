jQuery(function($) {
	$('#programjump').each(function() {
		var form = $(this);
		var select = form.find('select');
		var button = form.find('button');

		function updateCanSubmit() {
			if (select.val()) {
				form.attr('data-cansubmit', 'cansubmit');
			}
			else {
				form.removeAttr('data-cansubmit');
			}
		}

		select.on('change', updateCanSubmit);

		select.fancySelect();

		form.on('submit', function(e) {
			e.preventDefault();
			if (!select.val()) return;
			window.location = select.val();
		});

		updateCanSubmit();
	});
});
