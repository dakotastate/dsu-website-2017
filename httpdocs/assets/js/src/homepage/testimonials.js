function TestimonialsController(length) {
	this.index = 0;
	this.length = length;
	this.events = {};
}

TestimonialsController.prototype.getCurrentIndex = function() {
	return this.index;
};

TestimonialsController.prototype.moveNext = function() {
	var current = this.index;
	var target = (this.index + 1) % this.length;
	this.index = target;
	this._doChange(current, target);
};

TestimonialsController.prototype.movePrevious = function() {
	var current = this.index;
	var target = (this.index - 1 + this.length) % this.length;
	this.index = target;
	this._doChange(current, target);
};

TestimonialsController.prototype.moveTo = function(i) {
	if (i < 0 || i >= this.length) {
		return;
	}
	var current = this.index;
	var target = i;
	this.index = target;

	this._doChange(current, target);
};

TestimonialsController.prototype._doChange = function(current, target) {
	var self = this;

	if (current === target) {
		return;
	}

	self._triggerEvent('change', {
		currentIndex: current,
		targetIndex: target
	});
};

TestimonialsController.prototype.on = function(eventName, fun) {
	if (!this.events[eventName]) {
		this.events[eventName] = [];
	}

	this.events[eventName].push(fun);
};

TestimonialsController.prototype._triggerEvent = function(eventName, value) {
	var events = this.events[eventName];

	if (events) {
		for (var i = 0; i < events.length; i++) {
			events[i](value);
		}
	}
};

jQuery(function($) {
	var transitionEndEvent = 'transitionend webkitTransitionEnd oTransitionEnd MSTransitionEnd';

	$('.home-content .testimonials').each(function() {
		var testimonials = $(this);

		var canHandleTransition = $('html').is('.csstransitions.csstransforms');

		var controller = new TestimonialsController(testimonials.find('.testimonial').length);

		if (!canHandleTransition) {
			controller.on('change', function(data) {
				testimonials.find('.testimonial').eq(data.currentIndex).hide();
				testimonials.find('.testimonial').eq(data.targetIndex).show();
			});
		}
		else {
			controller.on('change', function(data) {
				testimonials.find('.testimonial').off(transitionEndEvent).removeClass('showing-start showing hiding-start hiding');
				var current = testimonials.find('.testimonial').eq(data.currentIndex);
				var target = testimonials.find('.testimonial').eq(data.targetIndex);

				var currentHeight = current.outerHeight();
				target.addClass('measuring');
				testimonials.css({ height: currentHeight + 'px' });
				var targetHeight = target.outerHeight();
				target.removeClass('measuring');

				current.addClass('hiding-start');
				current.removeClass('active');
				target.addClass('showing-start');
				target.addClass('active');

				setTimeout(function() {
					current.addClass('hiding');
					target.addClass('showing');
					testimonials.addClass('transitionheight');
					testimonials.css({ 'height': targetHeight + 'px' });
				}, 50);

				current.on(transitionEndEvent, function(e) {
					current.off(transitionEndEvent);
					current.removeClass('hiding-start hiding');
				});
				target.on(transitionEndEvent, function(e) {
					target.off(transitionEndEvent);
					target.removeClass('showing-start showing');
					testimonials.removeClass('transitionheight');
					testimonials.css({ height: 'auto' });
				});
			});
		}

		controller.on('change', function(data) {
			testimonials.find('.buttons button').removeClass('active');
			testimonials.find('.buttons button').eq(data.targetIndex).addClass('active');
		});

		testimonials.data('controller', controller);
		testimonials.find('.testimonial').eq(0).addClass('active');
		testimonials.addClass('transitionable');

		var buttons = $('<div>', {
			'class': 'buttons'
		});
		testimonials.find('.testimonial').each(function(index) {
			var button = $('<button>', {
				'class': 'testimonialbutton',
				'data-index': index,
				'tabindex': '-1'
			}).append('<span>Testimonial ' + (index + 1) + '</span>').appendTo(buttons);

			if (index === 0) {
				button.addClass('active');
			}
		});
		buttons.prependTo(testimonials);
		buttons.on('click', 'button', function(e) {
			var index = parseInt($(this).attr('data-index'), 10);
			controller.moveTo(index);
		});

		testimonials.attr('tabindex', '0');
		testimonials.on('keydown', function(e) {
			if (e.which === 37 || e.which === 39) {
				e.preventDefault();

				if (e.which === 37) {
					controller.movePrevious();
				}
				if (e.which === 39) {
					controller.moveNext();
				}
			}
		});
	});
});
