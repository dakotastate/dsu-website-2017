jQuery(function($) {
	var uniqueId = createUniqueIdGenerator('homepagetabs');

	var homeHappeningContent = $('.home-happening > .container > .sectioncontent');
	var sections = homeHappeningContent.find('> section');

	var tablist = $('<ul>', {
		'class': 'tablist',
		'role': 'tablist'
	});

	var isFirst = true;
	sections.each(function() {
		var section = $(this);

		var tabPanelId = section.attr('id');
		var tabListItemId = uniqueId();
		if (!tabPanelId) {
			tabPanelId = uniqueId();
			section.attr('id', tabPanelId);
		}

		section.attr('role', 'tabpanel');
		section.attr('aria-labelledby', tabListItemId);
		if (!isFirst) {
			section.hide();
		}

		var li = $('<li>', {
			'role': 'presentation'
		}).appendTo(tablist);
		var tabListItem = $('<a>', {
			'id': tabListItemId,
			'href': '#',
			'role': 'tab',
			'aria-controls': tabPanelId,
			'aria-selected': isFirst ? 'true' : 'false'
		}).appendTo(li);

		var sectionHeader = section.find('> header');
		tabListItem.text(sectionHeader.find('h1, h2, h3, h4, h5, h6').text());
		sectionHeader.remove();

		isFirst = false;
	});

	tablist.prependTo(homeHappeningContent);

	tablist.on('click', 'a', function(e) {
		var tabListItem = $(this);
		e.preventDefault();
		if (tabListItem.attr('aria-selected') === 'true') { return; }
		var tabPanel = $('#' + tabListItem.attr('aria-controls'));

		var currentlySelectedTabListItem = tablist.find('[aria-selected=true]');
		var currentlySelectedTabPanel = $('#' + currentlySelectedTabListItem.attr('aria-controls'));
		currentlySelectedTabListItem.attr('aria-selected', 'false');
		currentlySelectedTabPanel.hide();

		tabListItem.attr('aria-selected', 'true');
		tabPanel.show();
	});
});
