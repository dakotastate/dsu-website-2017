jQuery(function($) {
	$('.home-lead').each(function() {
		var carouselContainer = $(this).find('.sectioncontent ul');

		// IE11 seems to screw up when it gets high-definition images here. It
		// must have something to do with the intrinsic size of the images? Be
		// very picky about getting IE11, assuming IE12 will fix this (or use
		// <picture> entirely).
		if (window.devicePixelRatio > 1 && !(navigator.userAgent.match(/Trident\/7.0/) && navigator.userAgent.match(/rv:11\.0/))) {
			$(this).find('img[data-2x]').each(function() {
				$(this).attr('src', $(this).attr('data-2x'));
			});
		}

		function buildOrDestroyCarousel(mql) {
			function enableItem(item) {
				item.attr('aria-selected', 'true');
				item.attr('aria-hidden', 'false');
				item.find('a').attr('tabindex', '0');
			}

			function disableItem(item) {
				item.attr('aria-selected', 'false');
				item.attr('aria-hidden', 'true');
				item.find('a').attr('tabindex', '-1');
			}

			if (mql.matches) {
				var afterMoveFocusItem = false;
				var owl = null;
				carouselContainer.addClass('owl-carousel').attr('role', 'listbox').owlCarousel({
					singleItem: true,
					beforeMove: function carouselBeforeMove() {
						var previousItem = $(owl.$owlItems[owl.prevItem]);
						if (previousItem) {
							disableItem(previousItem);
						}
					},
					afterMove: function carouselAfterMove() {
						var currentItem = $(owl.$owlItems[owl.currentItem]);
						enableItem(currentItem);
					},
				});
				owl = carouselContainer.data('owlCarousel');
				carouselContainer.find('.owl-item').each(function(index) {
					var item = $(this);
					item.attr('role', 'option');
					disableItem(item);

					if (index === 0) {
						enableItem(item);
					}
				});

				$('<button>', {
					'text': 'Previous',
					'class': 'carouselbutton carouselbutton-previous'
				}).appendTo(carouselContainer.parent()).on('click', function(e) {
					carouselContainer.trigger('owl.prev');
				});

				$('<button>', {
					'text': 'Next',
					'class': 'carouselbutton carouselbutton-next'
				}).appendTo(carouselContainer.parent()).on('click', function(e) {
					carouselContainer.trigger('owl.next');
				});

				carouselContainer.on('keydown', function(e) {
					if (e.which === 37) {
						e.preventDefault();
						afterMoveFocusItem = true;
						carouselContainer.trigger('owl.prev');
					}
					if (e.which === 39) {
						e.preventDefault();
						afterMoveFocusItem = true;
						carouselContainer.trigger('owl.next');
					}
				});
			}
			else {
				var carouselObject = carouselContainer.data('owlCarousel');
				if (carouselObject) {
					carouselObject.destroy();
				}
				carouselContainer.removeClass('owl-carousel');
				carouselContainer.removeAttr('role');
				carouselContainer.parent().find('button').remove();
				carouselContainer.off('keydown');
			}
		}

		if (window.matchMedia) {
			var mql = window.matchMedia('(max-width: 47.9375em)'); /* 767/16 */
			mql.addListener(buildOrDestroyCarousel);
			buildOrDestroyCarousel(mql);
		}
	});
});
