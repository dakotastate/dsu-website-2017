jQuery(function($) {
	$('.sidebarcomponent-photogallery').each(function() {
		var component = $(this);
		var content = component.find('.sectioncontent');
		content.magnificPopup({
			delegate: 'a',
			type: 'image',
			gallery: {
				enabled: true
			},
			image: {
				titleSrc: function(item) {
					return item.el.find('img').attr('alt');
				}
			}
		});

		var button = $('<button>', {
			text: 'View Gallery'
		});
		button.on('click', function(e) {
			e.preventDefault();
			content.magnificPopup('open');
		});

		button.appendTo(content);
		component.addClass('scripted');
	});
});
