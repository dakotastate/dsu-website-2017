jQuery(function($) {
	FastClick.attach(document.body);

	// Sticky navigation
	var $html = $('html');
	var $window = $(window);
	var $pageheader = $('#page-header');
	var hasCampusAlertBox = !!$('#campusalert').length;
	function updateStickyNav(mql) {
		var isLargeScreen = mql.matches;
		var isScrolled = $window.scrollTop() > $pageheader.height();
		var isSearchBoxVisible = $html.is('.searchboxvisible');
		var isSticky = isLargeScreen && !hasCampusAlertBox && isScrolled && !isSearchBoxVisible;

		$html.toggleClass('stickynavigation', isSticky);
	}
	if (window.matchMedia) {
		var mql = window.matchMedia('(min-width: 48em)'); /* 767/16 */
		mql.addListener(updateStickyNav);
		updateStickyNav(mql);
		$window.on('scroll resize', function(e) {
			updateStickyNav(mql);
		});
	}

	// Featured image caption
	$('.content-featuredimage .caption').each(function() {
		var caption = $(this);
		var container = caption.closest('.content-featuredimage');
		container.addClass('scripted');

		var showButton = $('<button>', {
			'class': 'show',
			'aria-label': 'Show caption'
		});
		container.append(showButton);

		var hideButton = $('<button>', {
			'class': 'hide',
			'aria-label': 'Hide caption'
		});
		caption.append(hideButton);

		showButton.on('click', function(e) {
			e.preventDefault();
			container.addClass('visible');
			hideButton.focus();
		});

		hideButton.on('click', function(e) {
			e.preventDefault();
			container.removeClass('visible');
			showButton.focus();
		});
	});

	// Accessible menu
	$('#page-navigation .navigationroot > li').addClass('nav-item').find('> ul').addClass('sub-nav');
	$('#page-navigation').accessibleMegaMenu({
		menuClass: 'navigationroot',
		topNavItemClass: 'nav-item',
		panelClass: 'sub-nav'
	});

	// Skip to content
	$('#skiptocontent').on('click', function(e) {
		// When we skip to content, we also want to move the keyboard focus to
		// the primary content, so that the next tab doesn't go right back to
		// the logo. In order to do that, the content needs to be a focusable.
		// Since we pretty much know it's a <main> element and not something
		// that is focusable, we need to make it focusable by setting a
		// tabindex="-1" attribute on it. This allows us to focus it
		// programatically, but not the keyboard user.
		var content = $($('#skiptocontent').attr('href'));
		var tabindex = content.attr('tabindex');
		if (tabindex == null) {
			content.attr('tabindex', '-1');
		}
		content.focus();
	});

	$('.navigationroot > li > ul > .first a, #mobile-navigation .first a').each(function(i,el){
	  var url = $(this).attr('href');
	  var last_segment = url.split('/').pop();

	  if ( typeof alt_titles[last_segment] != 'undefined' ) {
	    $(this).text(alt_titles[last_segment]);
	  }
	});

});
