jQuery(function($) {
	if ($('.states-by-program .online-programs').length) {
		$('.states-by-program .online-programs').change(function() {
			$.ajax({
				type: 'GET',
				url: '/dynamic/states-by-online-program',
				dataType:'html',
				data: {
					program: $(this).val()
				},
				success:function(html) {
					$('.states-by-program .states-by-program-results').html(html);
				},
				error:function(x,t,e) {

				}
			});
		});
	}

	if ($('.programs-by-state .states').length) {
		$('.programs-by-state .states').change(function() {
			$.ajax({
				type: 'GET',
				url: '/dynamic/online-programs-by-state',
				dataType:'html',
				data: {
					state: $(this).val()
				},
				success:function(html) {
					$('.programs-by-state .programs-by-state-results').html(html);
				},
				error:function(x,t,e) {

				}
			});
		});
	}
});
