jQuery(function($) {
	$('.facultydirectory').each(function() {
		var directory = $(this);

		var search = directory.find('.search');
		var rdos = directory.find('[name=sortby]');

		prepareSearch(directory.find('.accordiongroup'));

		directory.clientsideSearch({
			inputSelector: 'input[type=search]',
			sectionsSelector: '.accordionitem',
			itemsSelector: '.facultydirectoryitem',
			indexAttribute: 'data-searchindex'//,
			//noResultsSelector: '.noresults span'
		});

		rdos.on('change', function(e) {
			var thing = $(this);
			var val = thing.val();

			switch (val) {
				case 'name':
					$('#facultydirectorycategory').text('Name');
					recreateAccordions(directory, 'data-name');
					break;
				case 'department':
					$('#facultydirectorycategory').text('Department');
					recreateAccordions(directory, 'data-department');
					break;
			}

			prepareSearch(directory.find('.accordiongroup'));
			directory.clientsideSearch('update');
		});
	});

	function prepareSearch(accordionGroup) {
		accordionGroup.find('.facultydirectoryitem').each(function() {
			var item = $(this);
			var header = item.find('h1, h2, h3, h4, h5, h6, button');
			item.attr('data-searchindex', header.text().toLowerCase());
			item.attr('data-ismatch', 'true');
		});
		accordionGroup.find('.accordionitem').each(function() {
			$(this).attr('data-containsmatch', 'true');
		});
	}

	function recreateAccordions(directory, property) {
		var groups = {};

		directory.find('.facultydirectoryitem').each(function() {
			var $this = $(this);
			var value = $this.attr(property);

			if (!groups[value]) {
				groups[value] = [];
			}

			groups[value].push($this);
		});

		var keys = Object.keys(groups);
		keys.sort();

		var accordionGroup = $('<div>', {
			'class': 'accordiongroup'
		});

		for (var i = 0; i < keys.length; i++) {
			var key = keys[i];

			var accordionItem = $('<section>', {
				'class': 'accordionitem'
			}).appendTo(accordionGroup);

			var header = $('<header>').appendTo(accordionItem);
			var h4 = $('<h4>').text(key).appendTo(header);
			var sectioncontent = $('<div>', {
				'class': 'sectioncontent'
			}).appendTo(accordionItem);

			var elements = groups[key];
			for (var j = 0; j < elements.length; j++) {
				sectioncontent.append(elements[j]);
			}
		}

		directory.find('.accordiongroup').remove();
		directory.append(accordionGroup);
		accordionGroup.accordionify({ animate: false });
	}
});
