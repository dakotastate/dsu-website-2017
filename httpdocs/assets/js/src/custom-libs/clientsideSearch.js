jQuery(function($) {
	var uniqueId = createUniqueIdGenerator('clientsidesearchdatalist-uid');

	$.fn.clientsideSearch = function(options) {
		if (typeof options == "string") {
			switch (options) {
				case "update":
					this.each(function() {
						var updatefn = $(this).data('clientsidesearch-updatefn');
						if (updatefn && typeof(updatefn) == "function") {
							updatefn();
						}
					});
					break;
				default:
					console.log("Unrecognized action '" + options + "' sent to clientsideSearch");
					break;
			}

			return;
		}

		options = options || {};
		var inputSelector = options.inputSelector;
		var sectionsSelector = options.sectionsSelector;
		var itemsSelector = options.itemsSelector;
		var indexAttribute = options.indexAttribute;
		var noResultsSelector = options.noResultsSelector;

		if (!inputSelector) {
			throw new Error("An inputSelector is required for clientsideSearch");
		}

		if (!sectionsSelector) {
			throw new Error("A sectionsSelector is required for clientsideSearch");
		}

		if (!itemsSelector) {
			throw new Error("A itemsSelector is required for clientsideSearch");
		}

		if (!indexAttribute) {
			throw new Error("An indexAttribute is required for clientsideSearch");
		}

		this.each(function() {
			var container = $(this);

			var input = container.find(inputSelector).eq(0);
			var sections = container.find(sectionsSelector);
			var programs = sections.find(itemsSelector);
			var noresultstext = container.find(noResultsSelector);

			var datalist = createList(programs);
			container.append(datalist);
			input.attr('list', datalist.attr('id'));

			var lastValue = null;
			input.on('keyup change search', function(e) {
				var value = input.val();
				if (lastValue !== value) {
					updateSearch(value.toLowerCase(), programs, sections);
					noresultstext.text(value);
				}
			});

			var update = function update() {
				lastValue = null;
				sections = container.find(sectionsSelector);
				programs = sections.find(itemsSelector);
				input = container.find(inputSelector).eq(0);

				updateSearch(input.val().toLowerCase(), programs, sections);
			};

			update();

			container.data('clientsidesearch-updatefn', update);
		});

		function updateSearch(text, items, sections) {
			if (text === '') {
				items.attr('data-ismatch', 'true');
				sections.attr('data-containsmatch', 'true');
				sections.each(function() {
					if ($(this).hasClass('accordionitem')) {
						$(this).attr('data-state', 'closed');
						$(this).children('.sectioncontent').hide();
					}
				});
			}
			else {
				// Split up the search into valid search words.
				var potentialTextParts = text.split(/\b/);
				var textParts = [];
				for (var i = 0; i < potentialTextParts.length; i++) {
					if (/^[a-zA-Z]+$/.test(potentialTextParts[i])) {
						textParts.push(potentialTextParts[i]);
					}
				}

				// Then, for each program, make sure it contains all of the valid
				// search words.
				items.each(function() {
					var program = $(this);
					var isMatch = true;
					for (var i = 0; i < textParts.length; i++) {
						var textPart = textParts[i];
						isMatch = isMatch && program.attr(indexAttribute).indexOf(textPart) >= 0;
						if (!isMatch) { break; }
					}
					program.attr('data-ismatch', isMatch.toString());
				});
				sections.each(function() {
					var section = $(this);
					var containsMatch = section.find('[data-ismatch=true]').length > 0;
					section.attr('data-containsmatch', containsMatch.toString());
					if ($(this).hasClass('accordionitem')) {
						section.attr('data-state', 'open');
						section.find('.sectioncontent').show();
					}
				});
			}
		}

		function createList(items) {
			var datalist = $('<datalist>', {
				id: uniqueId()
			});

			var keywordsLookup = {};

			items.each(function() {
				var item = $(this);
				var text = item.attr(indexAttribute).split(",");
				for (var i = 0; i < text.length; i++) {
					keywordsLookup[text[i].trim()] = true;
				}
			});

			var keywords = Object.keys(keywordsLookup);
			keywords.sort();

			for (var i = 0; i < keywords.length; i++) {
				var option = $('<option>', {
					value: keywords[i]
				});
				datalist.append(option);
			}

			return datalist;
		}
	};
});
