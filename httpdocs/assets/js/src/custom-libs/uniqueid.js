function createUniqueIdGenerator(prefix) {
	var i = 0;
	return function() {
		i++;
		return prefix + '-' + i;
	};
}
