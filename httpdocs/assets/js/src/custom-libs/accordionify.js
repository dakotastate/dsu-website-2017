jQuery(function($) {
	var uniqueId = createUniqueIdGenerator('accordion-uid');

	$.fn.accordionify = function(options) {
		options = options || {};
		var animate = true;

		if (options.animate !== undefined) {
			animate = !!options.animate;
		}

		this.each(function() {
			$(this).attr('role', 'tablist').attr('aria-multiselectable', 'true');

			$('.accordionitem', this).each(function() {
				var section = $(this);
				var header = section.find('header');
				var content = section.find('.sectioncontent');

				var tabId = uniqueId();
				var tabPanelId = uniqueId();

				content.attr('id', tabPanelId);
				content.attr('role', 'tabpanel');
				content.attr('aria-labelledby', tabId);
				content.attr('aria-hidden', 'true');

				section.attr('data-state', 'closed');

				// Why change the header to a button? Well, because then we get
				// keyboard navigation, focus, all sorts of stuff for free. And
				// because really, we're actually turning the header into a
				// button, so we might as well make the DOM reflect that.
				var h = header.find('h1, h2, h3, h4, h5, h6');
				var button = $('<button>', {
					text: h.text()
				});
				h.replaceWith(button);
				header.attr('id', tabId);
				header.attr('role', 'tab');
				header.attr('aria-expanded', 'false');
				header.attr('aria-controls', tabPanelId);

				if (animate) {
					content.slideUp();
				}
				else {
					content.hide();
				}
				header.on('click', function(e) {
					e.preventDefault();
					toggleProgram(section);
				});
			});
		});
	};

	function openProgram(section) {
		section.find('.sectioncontent').slideDown();
		section.attr('data-state', 'open');

		section.find('header button').attr('aria-expanded', 'true');
		section.find('.sectioncontent').attr('aria-hidden', 'false');
	}

	function closeProgram(section) {
		section.find('.sectioncontent').slideUp();
		section.attr('data-state', 'closed');

		section.find('header button').attr('aria-expanded', 'false');
		section.find('.sectioncontent').attr('aria-hidden', 'true');
	}

	function toggleProgram(section) {
		if (section.is('[data-state=open]')) {
			closeProgram(section);
		}
		else {
			openProgram(section);
		}
	}
});
