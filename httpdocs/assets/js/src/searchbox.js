jQuery(function($) {
	var transitionEndEvent = 'transitionend webkitTransitionEnd oTransitionEnd MSTransitionEnd';

	var html = $('html');
	var searchbox = $('#searchbox');
	var searchboxbutton = $('#searchboxbutton');
	var top = $('#page-top');

	function isVisible() {
		return html.is('.searchboxvisible');
	}

	function open() {
		html.addClass('searchboxvisible');
		searchbox.find('input').focus().select();

		if (html.is('.csstransitions.csstransforms')) {
			html.addClass('searchboxvisible-show-start');
			setTimeout(function() {
				html.addClass('searchboxvisible-show');
			}, 50);

			function transitionEnd(e) {
				if (e.target !== this) {
					return;
				}
				top.off('transitionend', transitionEnd);
				html.removeClass('searchboxvisible-show-start searchboxvisible-show');
			}
			top.on('transitionend', transitionEnd);
		}
	}

	function close() {
		html.removeClass('searchboxvisible');
		searchboxbutton.focus();

		if (html.is('.csstransitions.csstransforms')) {
			html.addClass('searchboxvisible-hide-start');
			setTimeout(function() {
				html.addClass('searchboxvisible-hide');
			}, 50);

			function transitionEnd(e) {
				if (e.target !== this) {
					return;
				}
				top.off('transitionend', transitionEnd);
				html.removeClass('searchboxvisible-hide-start searchboxvisible-hide');
			}
			top.on('transitionend', transitionEnd);
		}
	}

	function toggle() {
		if (isVisible()) {
			close();
		}
		else {
			open();
		}
	}

	searchboxbutton.on('click', function(e) {
		e.preventDefault();
		toggle();
	});

	searchbox.on('keydown', function(e) {
		if (e.which === 27 /* ESC */) {
			e.preventDefault();
			close();
		}
	});
});
