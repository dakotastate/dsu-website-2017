jQuery(function($) {
	var transitionEndEvent = 'transitionend webkitTransitionEnd oTransitionEnd MSTransitionEnd';

	var uniqueId = createUniqueIdGenerator('mobilenavigationscreen');

	var html = $('html');

	// Store which pane is currently visible. This is helpful for all sorts of
	// things, from easily determining which is visible, to allowing us to
	// write a general back function, because we know which one is visible.
	var currentPane = null;
	function setCurrentPane(pane) {
		currentPane = pane;
		mobileNavigation.attr('data-current', '#' + pane.attr('id'));
	}

	var originalNavigation = $('#page-navigation');
	var originalNavigationUl = originalNavigation.find('.navigationroot');
	var mobileNavigation = $('<nav>', {
		'role': 'navigation',
		'class': 'mobile',
		'id': 'mobile-navigation'
	});
	var mobileNavigationUl = originalNavigationUl.clone();
	mobileNavigationUl.appendTo(mobileNavigation);
	mobileNavigationUl.attr('id', uniqueId());
	mobileNavigationUl.find('ul').each(function() {
		var ul = $(this);
		var id = uniqueId();
		ul.attr('id', id);

		var parentUl = ul.parent().closest('ul');
		ul.attr('data-mobilenavigationparent', parentUl.attr('id'));

		// Add a direct link to the section we're in. When the user clicks on
		// a section that has children, they're taken to a display of those
		// children, not to the page itself. So, adding this direct link lets
		// them go directly to that page.
		var li = ul.parent();
		li.addClass('haschildren');
		var directLinkProtoA = li.find('> a');
		var directLinkLi = $('<li>', {
			'class': 'direct'
		});
		var directLinkA = directLinkProtoA.clone();
		directLinkProtoA.attr('data-mobilenavigationscreen', '#' + id);
		directLinkProtoA.attr('role', 'menuitem');
		directLinkA.appendTo(directLinkLi);
		directLinkLi.prependTo(ul);

		// Add a back button
		var backLinkButton = $('<button>', {
			'class': 'back',
			'text': 'Back'
		});
		var backLinkLi = $('<li>', {
			'class': 'back'
		});
		backLinkButton.appendTo(backLinkLi);
		backLinkLi.prependTo(ul);

		ul.appendTo(mobileNavigation);
	});

	$('nav.secondary .links li').each(function() {
		var li = $(this).clone();
		li.addClass('secondary');
		mobileNavigationUl.append(li);
	});

	var li = '<li class="secondary"><a href="' + $('a.portallink').attr('href') + '">' + $('a.portallink').html() + '</a></li>';
	mobileNavigationUl.append(li);

	var closeButton = $('<button>', {
		text: '',
		'aria-label': 'Close navigation',
		on: {
			click: function(e) {
				e.preventDefault();
				hideMobileNavigation();
				$("#mobilenavigationbutton").removeClass("active");
			}
		}
	});

	mobileNavigationUl.append($('<li>', { 'class': 'secondary' }).append(closeButton));

	mobileNavigation.on('click', 'button.back', function(e) {
		e.preventDefault();
		back();
	});

	mobileNavigation.on('click', 'a[data-mobilenavigationscreen]', function(e) {
		var link = $(this);
		var childScreenId = link.attr('data-mobilenavigationscreen');
		e.preventDefault();
		var childScreen = $(childScreenId);
		childScreen.addClass('animation-displaying');
		var parentScreen = $('#' + childScreen.attr('data-mobilenavigationparent'));
		childScreen.find('li.direct a').focus();
		setCurrentPane(childScreen);
		setTimeout(function() {
			childScreen.addClass('active');
		}, 50);
		childScreen.on(transitionEndEvent, function(e) {
			childScreen.off(transitionEndEvent);
			childScreen.removeClass('animation-displaying');
			parentScreen.addClass('obscured');
		});
	});

	mobileNavigationUl.addClass('active');
	mobileNavigation.insertAfter($('#page-top'));

	$('#mobilenavigationbutton').on('click', function(e) {
		if (html.is('.mobilenavigationvisible')) {
			hideMobileNavigation();
			$("#mobilenavigationbutton").removeClass("active");
		}
		else {
			showMobileNavigation();
			$(this).addClass("active");
		}
	});

	html.on('click', function(e) {
		if (html.is('.mobilenavigationvisible')
			&& !$.contains(mobileNavigation.get(0), e.target)
			&& e.target != html.get(0)) {

			var target = $(e.target);
			if (target.closest('#mobilenavigationbutton').length > 0) {
				return;
			}
			e.preventDefault();
			hideMobileNavigation();
			$("#mobilenavigationbutton").removeClass("active");
		}
	});

	mobileNavigation.on('keydown', function(e) {
		if (e.which === 27) {
			if (currentPane.attr('id') === mobileNavigationUl.attr('id')) {
				// If the root of the mobile naviation is visible, close the
				// mobile navigation.
				hideMobileNavigation();
				$("#mobilenavigationbutton").removeClass("active");
			}
			else {
				// If the root isn't visible, that means we're a couple of
				// layers deep in the navigation. ESC can go back one level.
				back();
			}
		}
	});

	function showMobileNavigation() {
		mobileNavigation.find('ul.active, ul.obscured').removeClass('active obscured');
		mobileNavigationUl.removeClass('obscured').addClass('active');
		html.addClass('mobilenavigationvisible');
		mobileNavigationUl.find('a').eq(0).focus();
		setCurrentPane(mobileNavigationUl);

		// Disable the rest of the screen.
		$('.bodywrapper2 > *').each(function() {
			var el = $(this);
			if (el.attr('id') !== mobileNavigation.attr('id')) {
				el.attr('aria-hidden', 'true');
			}
		});
	}

	function hideMobileNavigation() {
		// Enable the rest of the screen.
		$('.bodywrapper2 > *').each(function() {
			var el = $(this);
			el.removeAttr('aria-hidden');
		});

		html.addClass('mobilenavigationhiding-start');
		$('.bodywrapper2').on(transitionEndEvent, function(e) {
			if (e.target !== this) return;

			$('.bodywrapper2').off(transitionEndEvent);
			html.removeClass('mobilenavigationhiding-start mobilenavigationhiding mobilenavigationvisible');
		});
		setTimeout(function() {
			html.addClass('mobilenavigationhiding');
		}, 50);
		$('#mobilenavigationbutton').focus();
	}

	function back() {
		var thisScreen = currentPane;
		var parentScreen = $('#' + thisScreen.attr('data-mobilenavigationparent'));
		parentScreen.removeClass('obscured');
		thisScreen.removeClass('active');
		thisScreen.addClass('animation-hiding');
		thisScreen.on(transitionEndEvent, function(e) {
			thisScreen.off(transitionEndEvent);
			thisScreen.removeClass('active animation-hiding');
		});
		parentScreen.find('a').eq(0).focus();
		setCurrentPane(parentScreen);
	}

	// Additionally, the sidebar nav can be expanded and contracted.
	$('.sidebar-nav').each(function() {
		var sidebarnav = $(this);
		var sidebarbutton = $('<button>', {
			'aria-label': 'Show and hide section navigation',
			'class': 'mobile-section-navigation-button'
		}).append('<span></span><span></span><span></span>');

		// If the sidebar nav is empty, we don't want to create a dropdown.
		if (!sidebarnav.find('ul').length) { return; }

		// If we don't have a title element, something's probably wrong.
		if (!$('.content-title').length) { return; }

		$('.content-title').after(sidebarbutton);

		$('.content-title').on('click', function(e) {
			e.preventDefault();
			sidebarnav.slideToggle();
		});
		sidebarbutton.on('click', function(e) {
			e.preventDefault();
			sidebarnav.slideToggle();
		});

		if (window.matchMedia('(max-width: 47.9375em)').matches) { /* 767/16 */
			// If we're currently on the mobile view, slide the sidebar nav
			// up, so we don't create an unwanted jump.
			sidebarnav.slideUp();
		}
		else {
			// If we're not currently on the mobile view, just hide it
			// immediately. We have a CSS rule that overrides this hide for
			// large sizes, but it can't handle a slideUp well. So, if we just
			// hide this, then there is no disruption at larger sizes, but if
			// the screen does go down to a smaller size, this is hidden and
			// ready.
			sidebarnav.hide();
		}
	});
});
