jQuery(function($) {
	$('.form form').each(function() {
		var form = $(this);

		// FreeForm doesn't put the required class on the input itself. But we
		// do have it on the containing div. So, if it's on the containing
		// div, put it on the input.
		form.find('.field.required input, .field.required textarea, .field.required select').addClass('required');

		// Settings for validation.
		var settings = {};

		// If this is the contact form, send a Google Analytics event.
		if (window.location.pathname == '/contact/') {
			settings.submitHandler = function(form) {
				if (window.ga) {
					ga('send', 'event', 'Form', 'Submission', 'Contact');
				}
				form.submit();
			};
		}

		// And... validate.
		form.validate(settings);
	});
});
