<?php

// I got really sick of cutting out little diagonal lines. So, I created this
// thing that does all of the work of cutting those lines out, anyway.

$uri = $_SERVER['REQUEST_URI'];
$parts = explode('/', $uri);
// $parts[0] = ''
// $parts[1] = 'assets'
// $parts[2] = 'images'
// $parts[3] = 'diagonals'
// $parts[4] = '7': size
// $parts[5] = 'ff0000.png': color

if (count($parts) !== 6) {
	header("HTTP/1.0 404 Not Found");
	die();
}

// Size must be a number.
$sizestr = $parts[4];
if (!is_numeric($sizestr)) {
	header("HTTP/1.0 404 Not Found");
	die();
}

// Size must be a small number.
$imagesize = intval($sizestr);
if (!(0 < $imagesize && $imagesize < 50)) {
	header("HTTP/1.0 404 Not Found");
	die();
}

// Color must be a 6-digit hexidecimal number, followed by .png
$colorstr = $parts[5];
if (!preg_match('/[0-9a-f]{6}\.png/', $colorstr)) {
	header("HTTP/1.0 404 Not Found");
	die();
}
$colorstr = substr($colorstr, 0, 6);
$color = hex2rgb($colorstr);

header("Content-type: image/png");
// Seriously, this PNG is not gonna change.
header("Cache-Control: \"max-age=31536000, public\"");
header("Expires: ".gmdate('D, d M Y H:i:s \G\M\T', time() + 31536000));

$img = imagecreatetruecolor($imagesize, $imagesize);
$transparent = imagecolorallocate($img, 0, 0, 0);
imagecolortransparent($img, $transparent);

$imgcolor = imagecolorallocate($img, $color[0], $color[1], $color[2]);

// For whatever reason, the pixel pattern is:
// x----
// ----x
// ---x-
// --x--
// -x---
// With the pixel in the top left always set.
for ($x = 0; $x < $imagesize; $x++) {
	$y = $imagesize - $x;
	if ($x === 0) {
		$y = 0;
	}

	imagesetpixel($img, $x, $y, $imgcolor);
}

imagepng($img);
imagedestroy($img);

// Found this somewhere. Seems to work.
function hex2rgb($hex) {
   if(strlen($hex) == 3) {
      $r = hexdec(substr($hex,0,1).substr($hex,0,1));
      $g = hexdec(substr($hex,1,1).substr($hex,1,1));
      $b = hexdec(substr($hex,2,1).substr($hex,2,1));
   } else {
      $r = hexdec(substr($hex,0,2));
      $g = hexdec(substr($hex,2,2));
      $b = hexdec(substr($hex,4,2));
   }
   $rgb = array($r, $g, $b);
   return $rgb; // returns an array with the rgb values
}
