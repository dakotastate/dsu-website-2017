# Nimbus Base Install

---

## Database Dump

To dump the db for inclusion in the repo, run:

```mysqldump --add-drop-table --skip-comments -u [DB Username] [DB Name]```