module.exports = function(grunt) {

  grunt.initConfig({

    pkg: grunt.file.readJSON('package.json'),

    sass: {
      options: {
        outputStyle: 'compressed',
        sourceMap: true
      },
      dist: {
        files: [{
          expand: true,
          cwd: 'httpdocs/assets/css/src/',
          src: ['**/*.scss'],
          dest: 'httpdocs/assets/css/tmp/',
          ext: '.css'
        }]
      }
    },

    autoprefixer: {
      options: {
        map: true,
      },
      dist: {
        options: {
          browsers: ['> 1%', 'last 2 version', 'ie 9', 'android 2', 'android 4']
        },
        files: {
          'httpdocs/assets/css/style.css': 'httpdocs/assets/css/tmp/style.css',
        }
      },
      oldie: {
        options: {
          browsers: ['ie 8']
        },
        files: {
          'httpdocs/assets/css/old-ie.css': 'httpdocs/assets/css/tmp/old-ie.css',
        }
      },
      wufoo: {
        options: {
          browsers: ['> 1%', 'last 2 version', 'ie 8', 'ie 9', 'android 2', 'android 4']
        },
        files: {
          'httpdocs/assets/css/wufoo.css': 'httpdocs/assets/css/tmp/wufoo.css',
        }
      }
    },

    watch: {
      options: {
        livereload: true,
      },
      scss: {
        files: ['httpdocs/assets/css/src/**/*.scss'],
        tasks: ['compilescss'],
        options: {
          livereload: false,
        },
      },
      js: {
        files: ['httpdocs/assets/js/src/**/*.js'],
        tasks: ['uglify']
      },
      css: {
        files: ['httpdocs/assets/css/*.css'],
        tasks: []
      },
      html: {
        files: ['nimbus-local/templates/**/*.html'],
        tasks: []
      }
    },

    uglify: {
      dist: {
        options: {
          sourceMap: true
        },
        files: {
          'httpdocs/assets/js/script.js': [
            'httpdocs/assets/libs/fastclick-1.0.2/fastclick.js',
            'httpdocs/assets/libs/owlcarousel-1.3.2/owl.carousel.js',
            'httpdocs/assets/libs/matchMedia.js-0.2.0/matchMedia.js',
            'httpdocs/assets/libs/matchMedia.js-0.2.0/matchMedia.addListener.js',
            'httpdocs/assets/libs/magnific-popup/magnific-popup.js',
            'httpdocs/assets/libs/fancyselect-ad694472/fancySelect.js',

            'httpdocs/assets/js/src/polyfill/Object.keys.js',
            'httpdocs/assets/js/src/polyfill/String.prototype.trim.js',

            'httpdocs/assets/js/src/custom-libs/uniqueid.js',
            'httpdocs/assets/js/src/custom-libs/accordionify.js',
            'httpdocs/assets/js/src/custom-libs/clientsideSearch.js',
            'httpdocs/assets/js/src/mobile-navigation.js',
            'httpdocs/assets/js/src/searchbox.js',
            'httpdocs/assets/js/src/sidebars.js',
            'httpdocs/assets/js/src/forms.js',
            'httpdocs/assets/js/src/accordions.js',
            'httpdocs/assets/js/src/programs.js',
            'httpdocs/assets/js/src/program.js',
            'httpdocs/assets/js/src/staffdirectory.js',
            'httpdocs/assets/js/src/campusmap.js',
            'httpdocs/assets/js/src/script.js',
            'httpdocs/assets/js/src/homepage/tabs.js',
            'httpdocs/assets/js/src/homepage/lead.js',
            'httpdocs/assets/js/src/homepage/testimonials.js',
            'httpdocs/assets/js/src/online-programs.js'
          ]
        }
      }
    }
  });

  grunt.loadNpmTasks('grunt-autoprefixer');
  grunt.loadNpmTasks('grunt-sass');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-uglify');

  grunt.registerTask('compilescss', ['sass:dist', 'autoprefixer:dist', 'autoprefixer:oldie', 'autoprefixer:wufoo']);

  grunt.registerTask('default', ['compilescss', 'uglify']);
  grunt.registerTask('dev', ['watch']);
};
