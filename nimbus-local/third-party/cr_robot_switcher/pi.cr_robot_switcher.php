<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');



$plugin_info = array(
    'pi_name'       => 'CR Robot Switcher',
    'pi_version'    => '1.0',
    'pi_author'     => 'Click Rain',
    'pi_author_url' => '',
    'pi_description'=> 'Automatically switch robots.txt content based upon your environment. (Loosely based upon Carter Robots by Carter Digital)',
    'pi_usage'      => Cr_robot_switcher::usage()
);


class Cr_robot_switcher {

    public  $return_data;

    // Create production & development robots.txt containers
    private $robots_txt_production = '';
    private $robots_txt_development = '';

    // Define robots.txt fallback / default
    private $robots_txt_fallback = "### USING FALLBACK ###\n\nUser-agent: *\nDisallow: ";

    // Define Master Config non-production environments
    private $development_mc_environments = array('local','alpha','dev','development','beta','stage','staging');

    // Define development host segments
    private $development_host_segments = array('alpha','dev','development','beta','stage','staging');

    // Define development domains
    private $development_domains = array('clkra.in', 'clickrain.com');

    /**
     * Constructor
     */
    public function __construct()
    {
        // Instantiate EE
        $this->EE =& get_instance();

        // Load robots.txt data defined in the filesystem
        $this->load_robots();
    }

    private function load_robots()
    {
        $file_production  = $_SERVER['DOCUMENT_ROOT'] . '/robots-production.txt';
        $file_development = $_SERVER['DOCUMENT_ROOT'] . '/robots-development.txt';

        $this->robots_txt_production  = file_exists( $file_production )?  file_get_contents( $file_production ):  $this->robots_txt_fallback;
        $this->robots_txt_development = file_exists( $file_development )? file_get_contents( $file_development ): $this->robots_txt_fallback;
    }

    private function env_is_private( $environment )
    {
        if ( in_array( $environment, $this->development_mc_environments ) ) return TRUE;

        return FALSE;
    }

    private function host_is_private( $host )
    {
        $host_segments = explode( '.', $host );
        $domain_proper = implode( '.', array_slice( $host_segments, -2, 2 ) );

    	// Test First Host Segment
        if ( in_array( $host_segments[0], $this->development_host_segments ) ) return TRUE;

    	// Other Tests

    	// Test Last Two Host Segments (the domain proper)
        if ( in_array( $domain_proper, $this->development_domains ) ) return TRUE;

    	return FALSE;
    }

    public function txt()
    {
        // Set appropriate HTTP Content-Type header
        $this->EE->config->set_item('send_headers', FALSE);
        $this->EE->TMPL->template_type = 'cp_asset';
        $this->EE->output->set_header('Content-Type: text/plain; charset='.strtolower($this->EE->config->item('charset')) );

        // Check for Master Config environmental declaration
        if (defined('ENV')){

            if ( $this->env_is_private( ENV ) )
            {
                return '# MASTER CONFIG MATCH: ' . CR_REQ_HOST . "\n\n" . $this->robots_txt_development;
            } else {
                return '# MASTER CONFIG DEFAULT: ' . CR_REQ_HOST . "\n\n" . $this->robots_txt_production;
            }

        } else {

            // Try to automatically determine our environment by hostname
            if ( ! defined('CR_REQ_HOST') )
            {
				// Define Hostname
				define('CR_REQ_HOST', ( strpos($_SERVER['HTTP_HOST'],':') === FALSE )?
					$_SERVER['HTTP_HOST']:
					array_shift(explode(':',$_SERVER['HTTP_HOST'])) );
            }

            if ( $this->host_is_private( CR_REQ_HOST ) )
            {
                return '# AUTO-DETECT MATCH: ' . CR_REQ_HOST . "\n\n" . $this->robots_txt_development;
            } else {
                return '# AUTO-DETECT DEFAULT: ' . CR_REQ_HOST . "\n\n" . $this->robots_txt_production;
            }

        }

        // Return robots.txt data
		return $this->robots_txt_production;

    }

    // ----------------------------------------------------------------

    /**
     * Plugin Usage
     */
    public static function usage()
    {
        ob_start();
?>
Create a template called robots.txt.html in your site's primary group. It should contain only the following:

{exp:cr_robot_switcher:txt}

Also create a production and development robots.txt versions at the document root. They should be named "robots-production.txt" and "robots-development.txt", respectively.

This plugin will first check to see if you're using Focus Labs Master Config, and will serve an environmentally-appropriate robots.txt based upon the Master Config-defined environment (ENV).

If you're not using Master Config, the plugin will attempt to determine your environment by the current hostname.

The following hostnames will be automatically flagged as development environments:

* alpha
* beta
* dev
* development
* stage
* staging

All other hostnames will be treated as production environments, and will receive the production robots.txt.

Lists of Master Config environments and development hostnames are at the top of the plugin file. Adjust as necessary.

Please note that all of this will be overridden if an actual `robots.txt` file exists at document root.

<?php
        $buffer = ob_get_contents();
        ob_end_clean();
        return $buffer;
    }
}


/* End of file pi.cr_robot_switcher.php */
/* Location: /system/expressionengine/third_party/cr_robot_switcher/pi.cr_robot_switcher.php */