<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');



class Cr_nimbus_acc {

	var $name			= 'CR Nimbus Accessory';
	var $id				= 'cr_nimbus';
	var $version		= '1.1';
	var $description	= 'The Click Rain Nimbus Accessory.';
	var $settings       = array();
	var $sections		= array();
	var $assets_fpath   = './assets/nimbus/third-party/cr_nimbus/';
	var $assets_upath   = '/assets/nimbus/third-party/cr_nimbus/';
	var $css_upath, $css_fpath, $js_upath, $js_fpath, $img_upath, $img_fpath;
	var $uname,$ugroup;

	/**
	 * Constructor
	 */
	function __construct()
	{
		$this->EE =& get_instance();

		$this->settings    = array(
								'show_entry_nav'        => TRUE,
								'show_file_nav'         => TRUE,
								'show_addon_nav'        => TRUE,             // Super Admin Only
								'show_templates_nav'    => TRUE,             // Super Admin Only
								'entry_nav_style'       => 'structure',      // OPTIONS: structure, auto, custom

								// Configuration for all entry nav styles
								'entry_nav_title'       => 'Manage Content',

								// Configuration for "auto" entry nav style
								'singleton_channel_id'  => 1,
								'homepage_entry_id'     => 1,
								'admin_channel_order'   => array(2,3,4),
								'admin_channel_names'   => array(
									2 => 'Pages',
									3 => 'News Posts',
									4 => 'Events'
									),
								'gen_channel_order'     => array(3,2),
								'gen_channel_names'     => array(
									2 => 'Pages',
									3 => 'News Posts'
									),

								// Additional Top-Level Nav Items
								'extra_top_level_links' => array(
									array(
										'url'  => cp_url('addons_modules/show_module_cp', array('module' => 'freeform')),
										'name' => 'Forms',
										'icon' => 'cards-address'
										),
									),

								// Additional "Manage Content" Items
								'extra_content_links'   => array(

								//	array(
								//		'url'  => '', // Leave URL and name blank...
								//		'name' => ''  // to insert a divider.
								//		),
								//	array(
								//		'url'  => cp_url('addons_modules/show_module_cp', array('module' => 'structure')),'
								//		'name' => 'Page Structure'
								//		),

									),
								);

		$this->css_upath   = $this->assets_upath . 'css/';
		$this->img_upath   = $this->assets_upath . 'images/';
		$this->js_upath    = $this->assets_upath . 'js/';
		$this->css_fpath   = $this->assets_fpath . 'css/';
		$this->img_fpath   = $this->assets_fpath . 'images/';
		$this->js_fpath    = $this->assets_fpath . 'js/';

		$this->uname       = $this->EE->session->userdata('username');
		$this->ugroup      = $this->EE->session->userdata('group_id');
	}



	/**
	 * Set Sections
	 *
	 * Set content for the accessory
	 *
	 * @access	public
	 * @return	void
	 */
	function set_sections()
	{
		$css = '';
		$js = '';
		$ts = date('YmdHis');
		$_SITE_ID = $this->EE->config->item('site_id');



		/* ‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾ \
		\‾  CSS Injections  _\
		 \ ________________ */



		// Additional CSS for All Users
		if ( file_exists($this->css_fpath . 'cp.css') )
		{
			$css .= <<< EOD
				<link href="{$this->css_upath}cp.css?v={$ts}" type="text/css" title="default" rel="stylesheet" media="screen, projection">
EOD;
		}



		// Additional CSS for User Groups
		if ( file_exists( $this->css_fpath . "group-{$this->ugroup}-cp.css") )
		{
			$css .= <<< EOD
				<link href="{$this->css_upath}group-{$this->ugroup}-cp.css?v={$ts}" type="text/css" title="default" rel="stylesheet" media="screen, projection">
EOD;
		}



		// Additional CSS for Logged-in User
		if ( file_exists( $this->css_fpath . 'user-' . $this->uname . '-cp.css') )
		{
			$css .= <<< EOD
			<link href="{$this->css_upath}user-{$this->uname}-cp.css?v={$ts}" type="text/css" title="default" rel="stylesheet" media="screen, projection">
EOD;
		}



		// Inject CSS
		$this->EE->cp->add_to_head($css);






		/* ‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾ \
		\‾  JS Injections  _\
		 \ _______________ */



		// Additional JS for All Users
		if ( file_exists($this->js_fpath . 'cp.js') )
		{
			$js .= <<< EOD
				<script src="{$this->js_upath}cp.js?v={$ts}"></script>
EOD;
		}



		// Additional JS for User Groups
		if ( file_exists( $this->js_fpath . "group-{$this->ugroup}-cp.js") )
		{
			$js .= <<< EOD
				<script src="{$this->js_upath}group-{$this->ugroup}-cp.js?v={$ts}"></script>
EOD;
		}



		// Additional JS for Logged-in User
		if ( file_exists( $this->js_fpath . 'user-' . $this->uname . '-cp.js') )
		{
			$js .= <<< EOD
			<script src="{$this->js_upath}user-{$this->uname}-cp.js?v={$ts}"></script>
EOD;
		}



		// Inject JS
		$this->EE->cp->add_to_foot($js);






		/* ‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾ \
		\‾  Misc. Injections  _\
		 \ __________________ */



		// Inject favicon
		$this->EE->cp->add_to_head('<link rel="shortcut icon" type="image/png" href="' . $this->img_upath . 'favicon.png" />');



		// Add TypeKit
		/*
		$tk = <<< EOD
	<script type="text/javascript" src="http://use.typekit.com/dzn2nwn.js"></script>
	<script type="text/javascript">try{Typekit.load();}catch(e){}</script>
EOD;
		$this->EE->cp->add_to_head($tk);
		*/






		/* ‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾ \
		\‾  Primary Navigation Menu Changes  _\
		 \ _________________________________ */

		// Define menu var
		$channel_menu = '';

		// Create "Manage Content" Extra Links
		$entries_menu_extras = array();
		$entries_menu_class  = '';
		if ( isset($this->settings['extra_content_links']) && is_array($this->settings['extra_content_links']) && count($this->settings['extra_content_links']) > 0 ) {
			foreach($this->settings['extra_content_links'] as $l) {

				if ( $l['url'] == '' && $l['name'] == '' ) {
					// Insert a divider
					$entries_menu_extras[] = '<li class="nav_divider"></li>';
				} else {
					$eme_url = $l['url'];

					$entries_menu_extras[] = <<< EOD
						<li>
							<a href="{$eme_url}">{$l['name']}</a>
						</li>
EOD;
				}
			}
			$entries_menu_class = 'parent ';
		}
		$entries_menu_extras = implode($entries_menu_extras);

		// Create Extra Top Level Links
		$top_level_extras = array();
		if ( isset($this->settings['extra_top_level_links']) && is_array($this->settings['extra_top_level_links']) && count($this->settings['extra_top_level_links']) > 0 ) {
			foreach($this->settings['extra_top_level_links'] as $l) {
				$etl_url = $l['url'];
				$etl_icon = ( $l['icon'] == '' )? 'blog': $l['icon'];
				$top_level_extras[] = <<< EOD
					<li class=" has-icon">
						<a href="{$etl_url}" tabindex="-1" class="first_level">
							<img src="{$this->img_upath}icons/{$etl_icon}.png" alt="{$l['name']}" class="icon" /> {$l['name']}
						</a>
					</li>
EOD;
			}
		}
		$top_level_extras = implode($top_level_extras);



		// SWITCH: Use Structure-based Nav, or Publish / Edit based Nav
		switch ($this->settings['entry_nav_style'])
		{



			case 'structure':
				if ($entries_menu_extras != '') $entries_menu_extras = '<ul>'.$entries_menu_extras.'</ul>';
				$cp_url = cp_url('addons_modules/show_module_cp', array('module' => 'structure'));
				$channel_menu = <<< EOD
	<li class="{$entries_menu_class} has-icon">
		<a href="$cp_url" class="first_level" tabindex="-1">
			<img src="{$this->img_upath}icons/blog.png" alt="{$this->settings['entry_nav_title']}" class="icon" /> {$this->settings['entry_nav_title']}
		</a>{$entries_menu_extras}
	</li>
EOD;
			break;



			case 'auto':

				// Pull channel info
				switch ($this->ugroup)
				{

					// Pull ALL channels for Super Admins
					case 1:
						$cq = <<< EOQ
							SELECT
								`c`.`channel_title` AS `title`,
								`c`.`channel_name` AS `name`,
								`c`.`channel_id` AS `id`
							FROM
								`{$this->EE->db->dbprefix}channels` AS `c`
							WHERE
								`c`.`site_id` = '{$_SITE_ID}';
EOQ;
					break;

					// Pull group-accessible channels
					default:
						$cq = <<< EOQ
							SELECT
								`c`.`channel_title` AS `title`,
								`c`.`channel_name` AS `name`,
								`c`.`channel_id` AS `id`
							FROM
								`{$this->EE->db->dbprefix}channels` AS `c`,
								`{$this->EE->db->dbprefix}channel_member_groups` AS `cmg`
							WHERE
								`c`.`site_id` = '{$_SITE_ID}'
								&& `c`.`channel_id` = `cmg`.`channel_id`
								&& `cmg`.`group_id` = '{$this->EE->session->userdata('group_id')}';
EOQ;
					break;

				}

				$channels = array();
				foreach($this->EE->db->query($cq)->result_array() as $c)
				{
					$channels[$c['id']] = array(
						'name' => $c['name'],
						'id' => $c['id'],
						'ptitle' => $c['title'],
						'ctitle' => 'Create',
						'etitle' => 'Edit',
					);
				}

				// Define Homepage Content / Site Defaults Channel & Entry ID
				$homepage = array(
					'channel_id' => $this->settings['singleton_channel_id'],
					'entry_id'   => $this->settings['homepage_entry_id']
				);

				// Define / Reorder / Rename / Remove Channels by Member Group
				switch($this->ugroup)
				{
					// Super Admin Users
					case 1:
						$channel_order = $this->settings['admin_channel_order'];
						$channel_names = $this->settings['admin_channel_names'];
					break;

					// Everyone else
					default:
						$channel_order = $this->settings['gen_channel_order'];
						$channel_names = $this->settings['gen_channel_names'];
					break;
				}

				$channel_hide = array($homepage['channel_id']); // Hiding Homepage Content / Site Defaults SEC by default

				// Reorder
				if ( ! empty($channel_order) )
				{
					$new_channels = array();
					foreach($channel_order as $ci)
					{
						if ( isset($channels[$ci]) )
						{
							$new_channels[$channels[$ci]['id']] = $channels[$ci];
						}
					}
					$channels = $new_channels;
				}
				// Rename
				if ( ! empty($channel_names) )
				{
					foreach($channel_names as $ci => $cn){ if ( isset($channels[$ci]) ) $channels[$ci]['ptitle'] = $cn; };
				}
				// Remove
				if ( ! empty($channel_hide) )
				{
					foreach($channel_hide as $ci){ unset($channels[$ci]); }
				}

				foreach ($channels as $c => $ca)
				{
					$cl = isset($ca['ctitle'])?
						'<li class=""><a href="'.cp_url('content_publish/entry_form', array('channel_id' => $ca['id'])).'">'.$ca['ctitle'].'</a></li>':'';
					$el = isset($ca['etitle'])?
						'<li class=""><a href="'.cp_url('content_edit/index', array('channel_id' => $ca['id'])).'">'.$ca['etitle'].'</a></li>':'';

					$channel_menu .= <<< EOD
					<li class="parent">
						<a href="#" tabindex="-1">{$ca['ptitle']}</a>
						<ul>
							{$cl}
							{$el}
						</ul>
					</li>
EOD;
				}

				$channel_menu = <<< EOD
	<li class="parent has-icon">
		<a href="#" class="first_level" tabindex="-1">
			<img src="{$this->img_upath}icons/blog.png" alt="{$this->settings['entry_nav_title']}" class="icon" /> {$this->settings['entry_nav_title']}
		</a>
		<ul>
			{$channel_menu}
			{$entries_menu_extras}
		</ul>
	</li>
EOD;

			break;



			case 'custom':

				// Define your own!
				$channel_menu = <<< EOD
	<li class="parent has-icon">
		<a href="#" class="first_level" tabindex="-1">
			<img src="{$this->img_upath}icons/blog.png" alt="{$this->settings['entry_nav_title']}" class="icon" /> {$this->settings['entry_nav_title']}
		</a>
		<ul>
			<!-- Your Stuff Here -->
		</ul>
	</li>
EOD;

			break;

		}



		// Get File Upload Destinations
		$file_up_menu = '';

		switch ($this->ugroup)
		{
			case 1: // Pulling all upload destinations for Super Admins
		$fuq = <<< EOQ
	SELECT
		`fu`.`name` AS `name`,
		`fu`.`id` AS `id`
	FROM
		`{$this->EE->db->dbprefix}upload_prefs` AS `fu`
	WHERE
		`fu`.`site_id` = '{$_SITE_ID}';
EOQ;
			break;

			default: // Pulling user group restricted upload destinations
		$fuq = <<< EOQ
	SELECT
		`fu`.`name` AS `name`,
		`fu`.`id` AS `id`
	FROM
		`{$this->EE->db->dbprefix}upload_prefs` AS `fu`
	WHERE
		`fu`.`site_id` = '{$_SITE_ID}';
EOQ;
			break;
		}

		$file_ups = $this->EE->db->query('SELECT `name`, `id` FROM `'.$this->EE->db->dbprefix.'upload_prefs`;')->result();
		foreach ($this->EE->db->query($fuq)->result() as $fup)
		{
			$file_up_menu .= '<li class=""><a href="'.cp_url('content_files/index', array('dir_id' => $fup->id)).'">'.$fup->name.'</a></li>';
		}



		// Generate the Breadcrumb Menu

		//
		// THIS SECTION NEEDS TO BE FIXED FOR EE 2.8
		//

		$breadcrumb_menu = '<li>'.$this->EE->config->config['site_label'].'</li>';
		if (isset($_REQUEST['C']))
		{
			switch($_REQUEST['C'])
			{
				case 'content_publish':
				$breadcrumb_menu .= '<li>Manage Content</li>';
				break;

				case 'content_files':
				$breadcrumb_menu .= '<li>Manage Files</li>';
				break;

				case 'addons_modules':
					if (isset($_REQUEST['module']))
					{
						switch($_REQUEST['module'])
						{
							case 'republic_analytics':
							$breadcrumb_menu .= '<li>Site Statistics</li>';
							break;
							case 'cr_form_backup':
							$breadcrumb_menu .= '<li>Form Submissions</li>';
							break;
							case 'structure':
							$breadcrumb_menu .= '<li>Manage Content</li><li>Page Structure</li>';
							break;
						}
					}
				break;
			}
		}



		// Inject our changes into the menu for everyone...
		$menu = '';

		// Entry Nav
		if ( $this->settings['show_entry_nav'] )
		{
			$menu .= <<< EOD
		{$channel_menu}
EOD;
		}

		// File Upload Nav
		if ( $this->settings['show_file_nav'] )
		{
			$general_fup_url = cp_url('content_files/index');
			$menu .= <<< EOD
	<li class="parent manage-files has-icon">
		<a href="#" class="first_level" tabindex="-1">
			<img src="{$this->img_upath}icons/folder-open-image.png" alt="Manage Files" class="icon" /> Manage Files
		</a>
		<ul>
			{$file_up_menu}
			<li class=""><a href="$general_fup_url">Manage All Files</a></li>
		</ul>
	</li>
EOD;
		}



		// Append Extra Top Level Links to Menu
		if ( $top_level_extras != '' ) $menu .= $top_level_extras;



		// Inject our menu changes for the Super Admins
		if ($this->ugroup == 1)
		{
			// Add-on Nav
			if ( $this->settings['show_addon_nav'] )
			{
				$modules_url = cp_url('addons_modules/index');
				$accessories_url = cp_url('addons_accessories/index');
				$extensions_url = cp_url('addons_extensions/index');
				$fieldtypes_url = cp_url('addons_fieldtypes/index');
				$plugins_url = cp_url('addons_plugins/index');

				$menu .= <<< EOD
	<li class="parent manage-addons has-icon">
		<a href="#" tabindex="-1" class="first_level">
			<img src="{$this->img_upath}icons/gear.png" alt="Manage Add-Ons" class="icon" /> Add-Ons
		</a>
		<ul>
			<li class=""><a href="$modules_url">Modules</a></li>
			<li class=""><a href="$accessories_url">Accessories</a></li>
			<li class=""><a href="$extensions_url">Extensions</a></li>
			<li class=""><a href="$fieldtypes_url">Fieldtypes</a></li>
			<li class=""><a href="$plugins_url">Plugins</a></li>
		</ul>
	</li>
EOD;
			}

			// Templates Nav
			if ( $this->settings['show_templates_nav'] )
			{
				$templates_url = cp_url('design/manager');

				$menu .= <<< EOD
	<li class="manage-templates has-icon">
		<a href="$templates_url" tabindex="-1" class="first_level">
			<img src="{$this->img_upath}icons/blueprint.png" alt="Manage Templates" class="icon" /> Templates
		</a>
	</li>
	<li class="has-icon" id="expand-menu"><a href="#" tabindex="-1" class="first_level"><img src="{$this->img_upath}icons/terminal.png" alt="Expand Original Menu" class="icon" style="left:4px;" /></a></li>
EOD;
			}
		}

		$menu = str_replace("\n",'',$menu);
		$menu = str_replace("\r",'',$menu);



		// And now, finally, apply the menu changes.
		// We add the menu script to the head so that EE scripts apply to our changes.
		$js_menu = <<< EOD
		<script>
			jQuery(function(){
				var jQ = jQuery;
				var oldTabs = jQ('#navigationTabs > li:not(.home)');

				jQ('#navigationTabs').html('{$menu}').fadeIn(500);


				jQ(oldTabs).each(function(){
					jQ(this).addClass('hiddenMenu').appendTo('#navigationTabs');
				});

				jQ('#expand-menu a').click(function(){
					jQ('#navigationTabs .hiddenMenu').toggle();
					return false;
				});
			});
		</script>
EOD;
		$this->EE->cp->add_to_head($js_menu);






		/* ‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾ \
		\‾  Here there be dragons  _\
		 \ _______________________ */



		// Everyone!
		$breadcrumb_menu = str_replace("'","\'",$breadcrumb_menu);
		$js = <<< EOD
		<i id="group-id" class="group-{$this->EE->session->userdata('group_id')}"></i>
		<script>
			$(function(){
				$('#breadCrumb ol').html('{$breadcrumb_menu}');
				if ($('#publishForm').length && $('#publishForm input[name="entry_id"]').val() != '0') {
					$('#mainContent .contents .heading h2').text($('#mainContent .contents .heading h2').text() + ': ' + $('#hold_field_title input[name="title"]').val());
				}
			});
		</script>
EOD;

		// Append JS to page
		$this->EE->cp->add_to_foot($js);
	}



	function update()
	{
		return TRUE;
	}

}
// END CLASS
