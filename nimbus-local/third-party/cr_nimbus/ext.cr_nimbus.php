<?php

class Cr_nimbus_ext {

	var $name		= 'CR Nimbus';
	var $version 		= '1.0';
	var $description	= 'The Click Rain Nimbus Extension.';
	var $settings_exist	= 'y';
	var $docs_url		= ''; // 'http://expressionengine.com/user_guide/';

	var $settings 		= array();

	/**
	 * Constructor
	 *
	 * @param 	mixed	Settings array or empty string if none exist.
	 */
	function __construct($settings = '')
	{
		$this->EE =& get_instance();

		$this->settings = $settings;
	}



	function activate_extension()
	{
		$this->settings = array(
			'active' => 'y'
		);
		$hooks = array(
			'entry_submission_absolute_end'		=> 'on_publish',
			'wygwam_config'						=> 'wygwam_config'
		);
		foreach ($hooks as $h => $m)
		{
			$data = array(
				'class' => __CLASS__,
				'method' => $m,
				'hook' => $h,
				'settings' => serialize($this->settings),
				'priority' => 10,
				'version' => $this->version,
				'enabled' => $this->settings['active']
			);
			$this->EE->db->insert('extensions',$data);
		}
	}



	function disable_extension()
	{
		$this->EE->db->where('class', __CLASS__);
		$this->EE->db->delete('extensions');
	}



	function on_publish($a,$b,$c,$d)
	{
		$def_site = '1';		// Site ID (usually 1)
		$def_entry = '84';		// Defaults Entry ID


		// Finding Text / Textarea Fields
		$cfq = <<< EOQ
	SELECT
		`cf`.`field_name` AS `fname`,
		`cf`.`field_id` AS `fid`
	FROM
		`{$this->EE->db->dbprefix}channel_data` AS `cd`,
		`{$this->EE->db->dbprefix}channels` AS `c`,
		`{$this->EE->db->dbprefix}channel_fields` AS `cf`
	WHERE
		`cd`.`entry_id` = '{$def_entry}'
		&& `c`.`channel_id` = `cd`.`channel_id`
		&& `cf`.`group_id` = `c`.`field_group`
		&& ( `cf`.`field_type` = 'text' || `cf`.`field_type` = 'textarea' || `cf`.`field_type` = 'wygwam' || `cf`.`field_type` = 'pt_dropdown' );
EOQ;
		$g_fields = array();
		foreach ($this->EE->db->query($cfq)->result_array() as $cf)
		{
			$g_fields[str_replace('sd-','sdg-',$cf['fname'])] = $cf['fid'];
		}


		if ($a == $def_entry && $b['site_id'] == $def_site)
		{
			foreach ($g_fields as $t => $v)
			{
				$site_id = $b['site_id'];
				$var_val = $this->EE->db->escape_str($c['field_id_'.$v]);
				$q = $this->EE->db->query("SELECT `variable_id` FROM `exp_global_variables` WHERE `variable_name` = '{$t}' && `site_id` = '{$site_id}'");
				if ($q->num_rows() > 0)
				{
					$this->EE->db->query("UPDATE `exp_global_variables` SET `variable_data` = '{$var_val}' WHERE `variable_name` = '{$t}' && `site_id` = '{$site_id}'");
				}
				else
				{
					$this->EE->db->query("INSERT INTO `exp_global_variables` VALUES('','{$site_id}','{$t}','{$var_val}')");
				}
			}
		}
	}



	function wygwam_config($c,$s)
	{
		if ($this->EE->extensions->last_call !== FALSE)
		{
			$c = $this->EE->extensions->last_call;
		}
		/*
		$r = $this->EE->db->query("SELECT entry_id, title, url_title FROM `exp_channel_titles` WHERE `site_id` = 1 AND `channel_id` = 12;");
		if ( $r->num_rows() > 0 )
		{
			foreach ( $r->result_array() as $row )
			{
				$c['link_types']['Pop-up'][] = array(
					'label'		=> $row['title'],
					'url'		=> '/pop/'.$row['url_title'].'/',
					'class'		=> 'pop-box',
					'rel'		=> ''
				);
			}
		}
		*/

		return $c;
	}

}
// END CLASS

