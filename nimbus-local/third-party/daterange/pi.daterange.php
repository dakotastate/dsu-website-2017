<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$plugin_info = array(
	'pi_name'        => 'Date Range',
	'pi_version'     => '0.3.0',
	'pi_author'      => 'Click Rain',
	'pi_author_url'  => 'http://clickrain.com/',
	'pi_description' => 'Date Range - pleasantly readable date ranges',
	'pi_usage'       => Daterange::usage()
	);


class Daterange
{
	var $return_data = '';
	function Daterange()
	{
		Daterange::__construct();
	}

	function __construct() {
		$this->EE =& get_instance();

		$start_parameter = $this->EE->TMPL->fetch_param('start_date');
		$end_parameter = $this->EE->TMPL->fetch_param('end_date');
		$include_meta = strtolower($this->EE->TMPL->fetch_param('include_meta', 'yes'));
		switch ($include_meta) {
			case "no":
			case "n":
			case "false":
			case "f":
			case "0":
				$include_meta = FALSE;
				break;
			default:
				$include_meta = TRUE;
				break;
		}

		$start = new DateTime($this->EE->localize->format_date('%Y-%m-%dT%H:%i:%s%Q', $start_parameter));
		$end = new DateTime($this->EE->localize->format_date('%Y-%m-%dT%H:%i:%s%Q', $end_parameter));

		// If both times have a midnight time, then format them as dates only
		if ($start->format('H:i') == '00:00' && $end->format('H:i') == '00:00') {
			$this->return_data = $this->format_date($start, $end, $include_meta);
		}
		else {
			// Otherwise, the times matter, too.
			$this->return_data = $this->format_time($start, $end, $include_meta);
		}
	}

	private function format_date($start, $exclusive_end, $include_meta) {
		$end = new DateTime("@" . $exclusive_end->getTimestamp());
		$end->sub(new DateInterval('P1D'));

		$isostart = $start->format('Y-m-d\TH:i:00');
		// Google just takes the date from the string.
		// Even though ISO8601 considers `2013-03-22T00:00:00` and `2013-03-21T24:00:00` to be
		// the same instant, Google sees an endDate of the first as ending on 2013-03-22, and
		// an end date of the second as ending on 2013-03-21. So, to be the most correct,
		// an event from 2013-03-22 to 2013-03-24 would end on 2013-03-24T24:00:00.
		$isoend = $end->format('Y-m-d\T24:00:00');

		$years_equal = $start->format('Y') == $end->format('Y');
		$months_equal = $start->format('m') == $end->format('m');
		$days_equal = $start->format('d') == $end->format('d');

		$itemPropStart = '';
		$itemPropEnd = '';
		if ($include_meta) {
			$itemPropStart = ' itemprop="startDate"';
			$itemPropEnd = ' itemprop="endDate"';
		}

		$output = '';

		if ($years_equal && $months_equal && $days_equal) {
			$formatted = $start->format('F j, Y');
			$output = <<<EOF
<time datetime="{$isostart}"{$itemPropStart}>{$formatted}</time><meta{$itemPropEnd} content="{$isoend}">
EOF;
		}
		else if ($years_equal && $months_equal) {
			$formatted_start = $start->format('F j');
			$formatted_end = $end->format('j');
			$formatted_year = $start->format('Y');
			$output = <<<EOF
<time datetime="{$isostart}"{$itemPropStart}>{$formatted_start}</time>&ndash;<time datetime="{$isoend}"{$itemPropEnd}>{$formatted_end}</time>, {$formatted_year}
EOF;
		}
		else if ($years_equal) {
			$formatted_start = $start->format('F j');
			$formatted_end = $end->format('F j');
			$formatted_year = $start->format('Y');
			$output = <<<EOF
<time datetime="{$isostart}"{$itemPropStart}>{$formatted_start}</time> &ndash; <time datetime="{$isoend}"{$itemPropEnd}>{$formatted_end}</time>, {$formatted_year}
EOF;
		}
		else {
			$formatted_start = $start->format('F j, Y');
			$formatted_end = $end->format('F j, Y');
			$output = <<<EOF
<time datetime="{$isostart}"{$itemPropStart}>{$formatted_start}</time> &ndash; <time datetime="{$isoend}"{$itemPropEnd}>{$formatted_end}</time>
EOF;
		}

		return $output;
	}

	private function format_time($start, $end, $include_meta) {
		$isostart = $start->format('Y-m-d\TH:i:00');
		$isoend = $end->format('Y-m-d\TH:i:00');

		$years_equal = $start->format('Y') == $end->format('Y');
		$months_equal = $start->format('m') == $end->format('m');
		$days_equal = $start->format('d') == $end->format('d');

		$itemPropStart = '';
		$itemPropEnd = '';
		if ($include_meta) {
			$itemPropStart = ' itemprop="startDate"';
			$itemPropEnd = ' itemprop="endDate"';
		}

		$output = '';

		if ($years_equal && $months_equal && $days_equal) {

			$formatted_start = $start->format('F j, Y, g:i a');
			$formatted_end = $end->format('g:i a');
			$spaces = ' ';

			if ($start->format('i') == '00' && $end->format('i') == '00' && $start->format('a') == $end->format('a')) {
				$formatted_start = $start->format('F j, Y, g');
				$formatted_end = $end->format('ga');
				$spaces = '';
			}

			$output = <<<EOF
<time datetime="{$isostart}"{$itemPropStart}>{$formatted_start}</time>{$spaces}&ndash;{$spaces}<time datetime="{$isoend}"{$itemPropEnd}>{$formatted_end}</time>
EOF;
		}
		else if ($years_equal) {
			$formatted_start = $start->format('F j, g:i a');
			$formatted_end = $end->format('F j, g:i a');
			$formatted_year = $start->format('Y');
			$output = <<<EOF
<time datetime="{$isostart}"{$itemPropStart}>{$formatted_start}</time> &ndash; <time datetime="{$isoend}"{$itemPropEnd}>{$formatted_end}</time>, {$formatted_year}
EOF;
		}
		else {
			$formatted_start = $start->format('F j, Y, g:i a');
			$formatted_end = $end->format('F j, Y, g:i a');
			$output = <<<EOF
<time datetime="{$isostart}"{$itemPropStart}>{$formatted_start}</time> &ndash; <time datetime="{$isoend}"{$itemPropEnd}>{$formatted_end}</time>
EOF;
		}

		return $output;
	}

	/**
	 * Usage
	 *
	 * This function describes how the plugin is used.
	 *
	 * @access	public
	 * @return	string
	 */

	//  Make sure and use output buffering

	function usage()
	{
		ob_start();
?>




<?php
		$buffer = ob_get_contents();
		ob_end_clean();
		return $buffer;
	}
}
