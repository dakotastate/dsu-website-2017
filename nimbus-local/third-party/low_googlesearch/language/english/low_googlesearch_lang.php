<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Low GoogleSearch Language file
 *
 * @package        low_googlesearch
 * @author         Lodewijk Schutte <hi@gotolow.com>
 * @link           http://gotolow.com/addons/low-googlesearch
 * @copyright      Copyright (c) 2014, Low
 */
$lang = array(

"low_googlesearch_module_name" =>
"Low GoogleSearch",

"low_googlesearch_module_description" =>
"Integrates a Google Mini, Search Appliance or hosted Custom Search Engine",

// --------------------------------------------------------------------

''=>'');

// End low_googlesearch_lang.php