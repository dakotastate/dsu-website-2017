<?php

/**
 * Low GoogleSearch Config file
 *
 * @package        low_googlesearch
 * @author         Lodewijk Schutte <hi@gotolow.com>
 * @link           http://gotolow.com/addons/low-googlesearch
 * @copyright      Copyright (c) 2014, Low
 */

if ( ! defined('LOW_GS_NAME'))
{
	define('LOW_GS_NAME',    'Low GoogleSearch');
	define('LOW_GS_PACKAGE', 'low_googlesearch');
	define('LOW_GS_VERSION', '1.2.1');
	define('LOW_GS_DOCS',    'http://gotolow.com/addons/low-googlesearch');
}

/**
 * < EE 2.6.0 backward compat
 */
if ( ! function_exists('ee'))
{
	function ee()
	{
		static $EE;
		if ( ! $EE) $EE = get_instance();
		return $EE;
	}
}

/**
 * NSM Addon Updater
 */
$config['name']    = LOW_GS_NAME;
$config['version'] = LOW_GS_VERSION;
$config['nsm_addon_updater']['versions_xml'] = LOW_GS_DOCS.'/feed';