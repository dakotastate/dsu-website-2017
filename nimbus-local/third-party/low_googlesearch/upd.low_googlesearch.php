<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// Get config file
include(PATH_THIRD.'low_googlesearch/config.php');

/**
 * Low Google Search UPD class
 *
 * @package        low_googlesearch
 * @author         Lodewijk Schutte <hi@gotolow.com>
 * @link           http://gotolow.com/addons/low-googlesearch
 * @copyright      Copyright (c) 2014, Low
 */
class Low_googlesearch_upd {

	// --------------------------------------------------------------------
	// PROPERTIES
	// --------------------------------------------------------------------

	/**
	 * This version
	 *
	 * @access      public
	 * @var         string
	 */
	public $version = LOW_GS_VERSION;

	/**
	 * Class name
	 *
	 * @access      private
	 * @var         array
	 */
	private $class_name;

	/**
	 * Actions used
	 *
	 * @access      private
	 * @var         array
	 */
	private $actions = array(
		array('Low_googlesearch', 'redirect_to_results')
	);

	// --------------------------------------------------------------------
	// METHODS
	// --------------------------------------------------------------------

	/**
	 * Constructor
	 *
	 * @access     public
	 * @return     void
	 */
	public function __construct()
	{
		// Set class name
		$this->class_name = ucfirst(LOW_GS_PACKAGE);
	}

	// --------------------------------------------------------------------

	/**
	 * Install the module
	 *
	 * @access     public
	 * @return     bool
	 */
	public function install()
	{
		// --------------------------------------
		// Add row to modules table
		// --------------------------------------

		ee()->db->insert('modules', array(
			'module_name'    => $this->class_name,
			'module_version' => $this->version,
			'has_cp_backend' => 'n'
		));

		// --------------------------------------
		// Add rows to action table
		// --------------------------------------

		foreach ($this->actions AS $row)
		{
			list($class, $method) = $row;

			ee()->db->insert('actions', array(
				'class'  => $class,
				'method' => $method
			));
		}

		// --------------------------------------
		// csrf_exempt for EE >= 2.7
		// --------------------------------------

		$this->_csrf_exempt();

		return TRUE;
	}

	// --------------------------------------------------------------------

	/**
	 * Uninstall the module
	 *
	 * @access     public
	 * @return     bool
	 */
	public function uninstall()
	{
		// --------------------------------------
		// get module id
		// --------------------------------------

		$query = ee()->db->select('module_id')
		       ->from('modules')
		       ->where('module_name', $this->class_name)
		       ->get();

		// --------------------------------------
		// remove references from module_member_groups
		// --------------------------------------

		ee()->db->where('module_id', $query->row('module_id'));
		ee()->db->delete('module_member_groups');

		// --------------------------------------
		// remove references from modules
		// --------------------------------------

		ee()->db->where('module_name', $this->class_name);
		ee()->db->delete('modules');

		// --------------------------------------
		// remove references from actions
		// --------------------------------------

		ee()->db->where('class', $this->class_name);
		ee()->db->delete('actions');

		return TRUE;
	}

	// --------------------------------------------------------------------

	/**
	 * Update the module
	 *
	 * @access     public
	 * @return     bool
	 */
	public function update($current = '')
	{
		// -------------------------------------
		//  Same version? A-okay, daddy-o!
		// -------------------------------------

		if ($current == '' OR version_compare($current, $this->version) === 0)
		{
			return FALSE;
		}

		// Set csrf_exempt
		$this->_csrf_exempt();

		// Return TRUE to update version number in DB
		return TRUE;
	}

	// --------------------------------------------------------------------

	/**
	 * Set csrf_exempt to 1 in Actions table for EE2.7+
	 */
	private function _csrf_exempt()
	{
		if (version_compare(APP_VER, '2.7.0', '<')) return;

		list($class, $method) = $this->actions[0];

		ee()->db->update(
			'actions',
			array('csrf_exempt' => '1'),
			array('class' => $class, 'method' => $method)
		);
	}
}

// End upd.low_googlesearch.php