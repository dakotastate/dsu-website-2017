<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Low GoogleSearch MCP class
 *
 * @package        low_googlesearch
 * @author         Lodewijk Schutte <hi@gotolow.com>
 * @link           http://gotolow.com/addons/low-googlesearch
 * @copyright      Copyright (c) 2014, Low
 */
class Low_googlesearch_mcp {}

// End mcp.low_googlesearch.php