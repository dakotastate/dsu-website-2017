<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$plugin_info = array(
	'pi_name'        => 'Fmt',
	'pi_version'     => '0.2.0',
	'pi_author'      => 'Click Rain',
	'pi_author_url'  => 'http://clickrain.com/',
	'pi_description' => 'Format - Format things',
	'pi_usage'       => Fmt::usage()
	);


class Fmt
{
	var $return_data = '';
	function Fmt()
	{
		Fmt::__construct();
	}

	function __construct() {
		$this->EE =& get_instance();
	}

	public function phone($str = '') {
		require_once(__DIR__ . '/lib/formatphone.php');


		$format = $this->EE->TMPL->fetch_param('format');

		// Get the string that they want to escape from the template data
		$str = ($str == '') ? $this->EE->TMPL->tagdata : $str;

		$result = FormatPhone::format($str, $format);
		if (is_null($result)) {
			return $str;
		}

		return $result;
	}

	public function url($str = '') {
		require_once(__DIR__ . '/lib/formaturl.php');


		$include_http = $this->EE->TMPL->fetch_param('include_http', 'yes') == 'yes' ? TRUE : FALSE;

		// Get the string that they want to escape from the template data
		$str = ($str == '') ? $this->EE->TMPL->tagdata : $str;

		$result = FormatUrl::format($str, $include_http);
		if (is_null($result)) {
			return $str;
		}

		return $result;
	}

	/**
	 * Usage
	 *
	 * This function describes how the plugin is used.
	 *
	 * @access	public
	 * @return	string
	 */

	//  Make sure and use output buffering

	function usage()
	{
		ob_start();
?>

{exp:fmt:phone format="(000) 000-0000"}605.123.4567{/exp:fmt:phone}
=> (605) 123-4567

{exp:fmt:url include_http="yes"}google.com{/exp:fmt:url}
=> http://google.com

{exp:fmt:url include_http="no"}http://google.com{/exp:fmt:url}
=> google.com

{exp:fmt:url include_http="no"}https://google.com{/exp:fmt:url}
=> https://google.com

{exp:fmt:url include_http="yes"}https://google.com{/exp:fmt:url}
=> https://google.com

<?php
		$buffer = ob_get_contents();
		ob_end_clean();
		return $buffer;
	}
}
