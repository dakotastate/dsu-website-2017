<?php

class FormatUrl {
	public static function format($str, $includehttp) {
		if ($includehttp) {
			if (!preg_match("/^(http:\/\/|https:\/\/|mailto:)/", $str)) {
				return 'http://' . $str;
			}
			else {
				return NULL;
			}
		}

		if (!$includehttp) {
			if (preg_match('/^http:\/\//', $str)) {
				$result = substr($str, 7);

				# If the last character is a slash, and only the last character is a slash,
				# then take it off.
				if (substr_count($result, '/') == 1 && preg_match('/\/$/', $result)) {
					$result = substr($result, 0, strlen($result) - 1);
				}

				return $result;
			}
			else {
				return NULL;
			}
		}

		return NULL;
	}
}
