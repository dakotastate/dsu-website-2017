<?php

class FormatPhone {
	public static function format($str, $format) {
		$stripped = preg_replace('/\D/', '', $str);
		if (strlen($stripped) !== 10) {
			return null;
		}

		$formatstripped = preg_replace('/\D/', '', $format);
		if (strlen($formatstripped) !== 10) {
			return null;
		}

		$phonecount = 0;
		$result = '';
		for ($i=0; $i<strlen($format);$i++) {
			$c = $format[$i];
			if (is_numeric($c)) {
				$result .= $stripped[$phonecount];
				$phonecount++;
			}
			else {
				$result .= $c;
			}
		}

		return $result;
	}
}
