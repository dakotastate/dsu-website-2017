<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
/*
Description:        Brings Google Maps to ExpressionEngine as a Plugin
Developer:          Rein de Vries.
Location:           ./system/expressionengine/third_party/gmaps/pi.gmaps.php
Contact:            djrein86@hotmail.com
Version:            1.0.2

Changelog:			1.0.2 :
						Add zoom_control="yes"
						Add pan_control="yes"
						Add map_type_control="yes"
						Add scale_control="yes"
						Add street_view_control="yes"
						Changed map_type default to 'roadmap'
					1.0.1 : 
						Add map_type="Hybrid, Roadmap, Satellite or Terrain".
						Add Google Maps circle.
						Remove jQuery ready functions
					1.0 : init release.

Gmaps plugin 		http://hpneo.github.com/gmaps/

*/

$plugin_info = array(
  'pi_name' => 'Google maps plugin',
  'pi_version' => '1.0.2',
  'pi_author' => 'reinos.nl',
  'pi_author_url' => 'http://www.reinos.nl',
  'pi_description' => 'Give the user the power to use the Google Maps',
  'pi_usage' => Gmaps::usage()
);

class Gmaps
{
	private $EE; 
	private $site_id;
	
	public $return_data = '';
	
	/* Are we in development? */
	const DEVELOPMENT = false;

	/**
	 * Constructor
	 * 
	 * @return unknown_type
	 */
	function Gmaps()
	{
		//get EE object
		$this->EE =& get_instance();
		
		//sets site_id
		$this->site_id = $this->EE->config->item('site_id');  	

		//define the themes url
		if (defined('URL_THIRD_THEMES') === TRUE)
		{
			$theme_url = URL_THIRD_THEMES;
		}
		else
		{
			$theme_url = $this->EE->config->item('theme_folder_url').'third_party/';
		}

		// Are we working on SSL?
		if ((isset($_SERVER['HTTPS']) === TRUE && empty($_SERVER['HTTPS']) === FALSE) OR (isset($_SERVER['HTTP_HTTPS']) === TRUE && empty($_SERVER['HTTP_HTTPS']) === FALSE))
		{
			$theme_url = str_replace('http://', 'https://', $theme_url);
		}
	
		//set the theme url
		$this->theme_url = $theme_url . 'gmaps/';
		
		//set the theme path
		$this->theme_path = PATH_THEMES . '/third_party/gmaps/';	
	}

	// ----------------------------------------------------------------------------------
	
	/**
	 * _init
	 * 
	 * @return unknown_type
	 */
	private function _init()
	{
		//Add base files, you have to do this when youre first form is in a {if}{/if} and is not visible. (The plugin will run even when the if is not true.)
		$this->add_base_files = $this->EE->TMPL->fetch_param('add_base_files') == 'yes' ? true : false ;
		
		$this->div_id = $this->EE->TMPL->fetch_param('id') != '' ? $this->EE->TMPL->fetch_param('id') : '' ;
		$this->div_class = $this->EE->TMPL->fetch_param('class') != '' ? $this->EE->TMPL->fetch_param('class') : '' ;
		
		//what is the selector
		if($this->div_id == '' && $this->div_class != '')
		{
			$this->selector = '.'.$this->div_class;
			$this->div_id = 'ee_gmap';
		}
		else if($this->div_id != '' && $this->div_class == '')
		{
			$this->selector = '#'.$this->div_id;
			$this->div_class = 'ee_gmap_class';
		}
		else if($this->div_id != '' && $this->div_class != '')
		{
			$this->selector = '#'.$this->div_id;
		}
		else
		{
			$this->selector = '#ee_gmap';
			$this->div_id = 'ee_gmap';
		}
		
		//set the caller times
		$this->_add_call_timer();
		
		//add the css and js files
		if($this->caller_id == 1) 
		{			
			
			//minify css on dev only
			if ( strstr( $_SERVER['SERVER_NAME'], '.local' ) && self::DEVELOPMENT)
			{
				$this->_compress_js(array(
					$this->theme_url . 'js/gmaps.js',
					$this->theme_url . 'js/ee_gmaps.js'
				));
				
				//add js
				$this->return_data .= '
					<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=true"></script>
					<script type="text/javascript" src="' . $this->theme_url . 'js/gmaps.js" ></script>
					<script type="text/javascript" src="' . $this->theme_url . 'js/ee_gmaps.js" ></script>
				';
			}
			else
			{
				//add js
				$this->return_data .= '
					<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=true"></script>
					<script type="text/javascript" src="' . $this->theme_url . 'js/gmaps.min.js" ></script>
				';
			}	
		}	
	}
	
	// ----------------------------------------------------------------------------------

	/**
	 * geocoding
	 * 
	 * @return unknown_type
	 */
	function geocoding()
	{
		//call the init function to init some default values
		$this->_init();
	
		//set the class
		$this->address = $this->EE->TMPL->fetch_param('address') != '' ? $this->_build_js_array($this->EE->TMPL->fetch_param('address')) : '' ;
		$this->map_type = $this->EE->TMPL->fetch_param('map_type') != '' ? $this->EE->TMPL->fetch_param('map_type') : 'roadmap' ;
		$this->width = $this->EE->TMPL->fetch_param('width') != '' ? $this->EE->TMPL->fetch_param('width') : 400 ;
		$this->height = $this->EE->TMPL->fetch_param('height') != '' ? $this->EE->TMPL->fetch_param('height') : 400 ;
		$this->zoom = $this->EE->TMPL->fetch_param('zoom') != '' ? $this->EE->TMPL->fetch_param('zoom') : 15 ;
		$this->marker = $this->EE->TMPL->fetch_param('marker') == 'no' ? 'false' : 'true' ;
		$this->marker_icon = $this->EE->TMPL->fetch_param('marker_icon') != '' ? $this->EE->TMPL->fetch_param('marker_icon') : '' ;
		$this->marker_html = $this->EE->TMPL->fetch_param('marker_html') != '' ? $this->EE->TMPL->fetch_param('marker_html') : '' ;
		$this->custom_marker_html = $this->EE->TMPL->fetch_param('custom_marker_html') != '' ? $this->EE->TMPL->fetch_param('custom_marker_html') : '';
		$this->custom_marker_vertical_align = $this->EE->TMPL->fetch_param('custom_marker_vertical_align') != '' ? $this->EE->TMPL->fetch_param('custom_marker_vertical_align') : 'top';
		$this->custom_marker_horizontal_align = $this->EE->TMPL->fetch_param('custom_marker_horizontal_align') != '' ? $this->EE->TMPL->fetch_param('custom_marker_horizontal_align') : 'center';
		$this->static = $this->EE->TMPL->fetch_param('static') == 'yes' ? 'true' : 'false' ;
		$this->zoom_control = $this->EE->TMPL->fetch_param('zoom_control') == 'no' ? 'false' : 'true' ;
		$this->pan_control = $this->EE->TMPL->fetch_param('pan_control')  == 'no' ? 'false' : 'true' ;
		$this->map_type_control = $this->EE->TMPL->fetch_param('map_type_control') == 'no' ? 'false' : 'true' ;
		$this->scale_control = $this->EE->TMPL->fetch_param('scale_control')  == 'no' ? 'false' : 'true' ;
		$this->street_view_control = $this->EE->TMPL->fetch_param('street_view_control') == 'no' ? 'false' : 'true' ;
	
		//if address empty return a error.
		if($this->address == '') 
		{
			return 'You forgot to fill in an address';
		}

		//return a div
		$this->return_data .= '<div id="'.$this->div_id.'" class="'.$this->div_class.'"></div>';
		
		//return the js 
		$this->return_data .= '<script type="text/javascript">
			EE_GMAPS.setGeocoding({
				selector : "'.$this->selector.'",
				address : '.$this->address.',
				map_type : "'.$this->map_type.'",
				width : '.$this->width.',
				height : '.$this->height.',
				zoom : '.$this->zoom.',
				marker : '.$this->marker.',
				marker_icon : "'.$this->marker_icon.'",
				marker_html : "'.$this->marker_html.'",
				custom_marker_html : "'.$this->custom_marker_html.'",
				custom_marker_vertical_align : "'.$this->custom_marker_vertical_align.'",
				custom_marker_horizontal_align : "'.$this->custom_marker_horizontal_align.'",
				static : '.$this->static.',
				zoom_control : '.$this->zoom_control.',
				pan_control : '.$this->pan_control.',
				map_type_control : '.$this->map_type_control.',
				scale_control : '.$this->scale_control.',
				street_view_control : '.$this->street_view_control.'
			});	
		</script>';
	
		//return the gmaps
		return $this->return_data;
	}

	// ----------------------------------------------------------------------------------
	
	/**
	 * route
	 * 
	 * @return unknown_type
	 */
	function route()
	{
		//call the init function to init some default values
		$this->_init();
	
		//set the vars
		$this->from_address = $this->EE->TMPL->fetch_param('from_address') != '' ? $this->EE->TMPL->fetch_param('from_address') : '' ;
		$this->to_address = $this->EE->TMPL->fetch_param('to_address') != '' ? $this->EE->TMPL->fetch_param('to_address') : '' ;
		$this->map_type = $this->EE->TMPL->fetch_param('map_type') != '' ? $this->EE->TMPL->fetch_param('map_type') : 'roadmap' ;
		$this->travel_mode = $this->EE->TMPL->fetch_param('travel_mode') != '' ? $this->EE->TMPL->fetch_param('travel_mode') : 'driving' ;
		$this->stroke_color = $this->EE->TMPL->fetch_param('stroke_color') != '' ? $this->EE->TMPL->fetch_param('stroke_color') : '#131540' ;
		$this->stroke_opacity = $this->EE->TMPL->fetch_param('stroke_opacity') != '' ? $this->EE->TMPL->fetch_param('stroke_opacity') : '0.6' ;
		$this->stroke_weight = $this->EE->TMPL->fetch_param('stroke_weight') != '' ? $this->EE->TMPL->fetch_param('stroke_weight') : '6' ;
		$this->markers = $this->EE->TMPL->fetch_param('markers') == 'no' ? 'false' : 'true' ;
		$this->marker_icon = $this->EE->TMPL->fetch_param('marker_icon') != '' ? $this->EE->TMPL->fetch_param('marker_icon') : '' ;
		$this->width = $this->EE->TMPL->fetch_param('width') != '' ? $this->EE->TMPL->fetch_param('width') : 400 ;
		$this->height = $this->EE->TMPL->fetch_param('height') != '' ? $this->EE->TMPL->fetch_param('height') : 400 ;
		$this->stops = $this->EE->TMPL->fetch_param('stops') != '' ? $this->_build_js_array($this->EE->TMPL->fetch_param('stops')) : '[]' ;
		$this->zoom_control = $this->EE->TMPL->fetch_param('zoom_control') == 'no' ? 'false' : 'true' ;
		$this->pan_control = $this->EE->TMPL->fetch_param('pan_control')  == 'no' ? 'false' : 'true' ;
		$this->map_type_control = $this->EE->TMPL->fetch_param('map_type_control') == 'no' ? 'false' : 'true' ;
		$this->scale_control = $this->EE->TMPL->fetch_param('scale_control')  == 'no' ? 'false' : 'true' ;
		$this->street_view_control = $this->EE->TMPL->fetch_param('street_view_control') == 'no' ? 'false' : 'true' ;

		//if address empty return a error.
		if($this->to_address == '') 
		{
			return 'You forgot to fill in an to address';
		}
		if($this->from_address == '') 
		{
			return 'You forgot to fill in an from address';
		}

		//return a div
		$this->return_data .= '<div id="'.$this->div_id.'" class="'.$this->div_class.'"></div>';
		
		//return the js 
		$this->return_data .= '<script type="text/javascript">
			EE_GMAPS.setRoute({
				selector : "'.$this->selector.'", 
				from_address : "'.$this->from_address.'", 
				to_address : "'.$this->to_address.'", 
				map_type : "'.$this->map_type.'",
				stopsaddresses : '.$this->stops.',
				travel_mode: "'.$this->travel_mode.'",
				stroke_color: "'.$this->stroke_color.'",
				stroke_opacity: '.$this->stroke_opacity.',
				stroke_weight: '.$this->stroke_weight.',
				markers: '.$this->markers.',
				marker_icon: "'.$this->marker_icon.'",
				width : '.$this->width.',
				height : '.$this->height.',
				zoom_control : '.$this->zoom_control.',
				pan_control : '.$this->pan_control.',
				map_type_control : '.$this->map_type_control.',
				scale_control : '.$this->scale_control.',
				street_view_control : '.$this->street_view_control.'
			});
		</script>';
	
		//return the gmaps
		return $this->return_data;
	}

	// ----------------------------------------------------------------------------------

	/**
	 * polygon
	 * 
	 * @return unknown_type
	 */
	function polygon()
	{
		//call the init function to init some default values
		$this->_init();
	
		//set the class
		$this->address = $this->EE->TMPL->fetch_param('address') != '' ? $this->_build_js_array($this->EE->TMPL->fetch_param('address')) : '[]';
		$this->map_type = $this->EE->TMPL->fetch_param('map_type') != '' ? $this->EE->TMPL->fetch_param('map_type') : 'roadmap' ;
		$this->width = $this->EE->TMPL->fetch_param('width') != '' ? $this->EE->TMPL->fetch_param('width') : 400 ;
		$this->height = $this->EE->TMPL->fetch_param('height') != '' ? $this->EE->TMPL->fetch_param('height') : 400 ;
		$this->zoom = $this->EE->TMPL->fetch_param('zoom') != '' ? $this->EE->TMPL->fetch_param('zoom') : 15 ;
		$this->stroke_color = $this->EE->TMPL->fetch_param('stroke_color') != '' ? $this->EE->TMPL->fetch_param('stroke_color') : '#BBD8E9' ;
		$this->stroke_opacity = $this->EE->TMPL->fetch_param('stroke_opacity') != '' ? $this->EE->TMPL->fetch_param('stroke_opacity') : 1 ;
		$this->stroke_weight = $this->EE->TMPL->fetch_param('stroke_weight') != '' ? $this->EE->TMPL->fetch_param('stroke_weight') : 3 ;
		$this->fill_color = $this->EE->TMPL->fetch_param('fill_color') != '' ? $this->EE->TMPL->fetch_param('fill_color') : '#BBD8E9' ;
		$this->fill_opacity = $this->EE->TMPL->fetch_param('fill_opacity') != '' ? $this->EE->TMPL->fetch_param('fill_opacity') : 0.6 ;
		$this->marker = $this->EE->TMPL->fetch_param('marker') == 'no' ? 'false' : 'true' ;
		$this->marker_icon = $this->EE->TMPL->fetch_param('marker_icon') != '' ? $this->EE->TMPL->fetch_param('marker_icon') : '' ;
		$this->static = $this->EE->TMPL->fetch_param('static') == 'yes' ? 'true' : 'false' ;
		$this->zoom_control = $this->EE->TMPL->fetch_param('zoom_control') == 'no' ? 'false' : 'true' ;
		$this->pan_control = $this->EE->TMPL->fetch_param('pan_control')  == 'no' ? 'false' : 'true' ;
		$this->map_type_control = $this->EE->TMPL->fetch_param('map_type_control') == 'no' ? 'false' : 'true' ;
		$this->scale_control = $this->EE->TMPL->fetch_param('scale_control')  == 'no' ? 'false' : 'true' ;
		$this->street_view_control = $this->EE->TMPL->fetch_param('street_view_control') == 'no' ? 'false' : 'true' ;

		//if address empty return a error.
		if($this->address == '') 
		{
			return 'You forgot to fill in an address';
		}

		//return a div
		$this->return_data .= '<div id="'.$this->div_id.'" class="'.$this->div_class.'"></div>';
		
		//return the js 
		$this->return_data .= '<script type="text/javascript">
			EE_GMAPS.setPolygon({
				selector : "'.$this->selector.'",
				address : '.$this->address.',
				map_type : "'.$this->map_type.'",
				width : '.$this->width.',
				height : '.$this->height.',
				zoom : '.$this->zoom.',
				stroke_color : "'.$this->stroke_color.'",
				stroke_opacity : '.$this->stroke_opacity.',
				stroke_weight : '.$this->stroke_weight.',
				fill_color : "'.$this->fill_color.'",
				fill_opacity : '.$this->fill_opacity.',
				marker : '.$this->marker.',
				marker_icon : "'.$this->marker_icon.'",
				static : '.$this->static.',
				zoom_control : '.$this->zoom_control.',
				pan_control : '.$this->pan_control.',
				map_type_control : '.$this->map_type_control.',
				scale_control : '.$this->scale_control.',
				street_view_control : '.$this->street_view_control.'
			});
		</script>';
	
		//return the gmaps
		return $this->return_data;
	}

	// ----------------------------------------------------------------------------------

	/**
	 * geolocation
	 * 
	 * @return unknown_type
	 */
	function circle()
	{
		//call the init function to init some default values
		$this->_init();
		
		//set the class
		$this->address = $this->EE->TMPL->fetch_param('address') != '' ? $this->_build_js_array($this->EE->TMPL->fetch_param('address')) : '' ;
		$this->map_type = $this->EE->TMPL->fetch_param('map_type') != '' ? $this->EE->TMPL->fetch_param('map_type') : 'roadmap' ;
		$this->width = $this->EE->TMPL->fetch_param('width') != '' ? $this->EE->TMPL->fetch_param('width') : 400 ;
		$this->height = $this->EE->TMPL->fetch_param('height') != '' ? $this->EE->TMPL->fetch_param('height') : 400 ;
		$this->zoom = $this->EE->TMPL->fetch_param('zoom') != '' ? $this->EE->TMPL->fetch_param('zoom') : 15 ;
		$this->marker = $this->EE->TMPL->fetch_param('marker') == 'no' ? 'false' : 'true' ;
		$this->marker_icon = $this->EE->TMPL->fetch_param('marker_icon') != '' ? $this->EE->TMPL->fetch_param('marker_icon') : '' ;
		$this->fit_circle = $this->EE->TMPL->fetch_param('fit_circle') == 'no' ? 'false' : 'true' ;
		$this->stroke_color = $this->EE->TMPL->fetch_param('stroke_color') != '' ? $this->EE->TMPL->fetch_param('stroke_color') : '#BBD8E9' ;
		$this->stroke_opacity = $this->EE->TMPL->fetch_param('stroke_opacity') != '' ? $this->EE->TMPL->fetch_param('stroke_opacity') : 1 ;
		$this->stroke_weight = $this->EE->TMPL->fetch_param('stroke_weight') != '' ? $this->EE->TMPL->fetch_param('stroke_weight') : 3 ;
		$this->fill_color = $this->EE->TMPL->fetch_param('fill_color') != '' ? $this->EE->TMPL->fetch_param('fill_color') : '#BBD8E9' ;
		$this->fill_opacity = $this->EE->TMPL->fetch_param('fill_opacity') != '' ? $this->EE->TMPL->fetch_param('fill_opacity') : 0.6 ;
		$this->radius = $this->EE->TMPL->fetch_param('radius') != '' ? $this->EE->TMPL->fetch_param('radius') : 1000 ;
		$this->zoom_control = $this->EE->TMPL->fetch_param('zoom_control') == 'no' ? 'false' : 'true' ;
		$this->pan_control = $this->EE->TMPL->fetch_param('pan_control')  == 'no' ? 'false' : 'true' ;
		$this->map_type_control = $this->EE->TMPL->fetch_param('map_type_control') == 'no' ? 'false' : 'true' ;
		$this->scale_control = $this->EE->TMPL->fetch_param('scale_control')  == 'no' ? 'false' : 'true' ;
		$this->street_view_control = $this->EE->TMPL->fetch_param('street_view_control') == 'no' ? 'false' : 'true' ;

		//if address empty return a error.
		if($this->address == '') 
		{
			return 'You forgot to fill in an address';
		}

		//return a div
		$this->return_data .= '<div id="'.$this->div_id.'" class="'.$this->div_class.'"></div>';
		
		//return the js 
		$this->return_data .= '<script type="text/javascript">	
			EE_GMAPS.setCircle({
				selector : "'.$this->selector.'",
				address : '.$this->address.',
				map_type : "'.$this->map_type.'",
				width : '.$this->width.',
				height : '.$this->height.',
				zoom : '.$this->zoom.',
				marker : '.$this->marker.',
				marker_icon : "'.$this->marker_icon.'",
				fit_circle : '.$this->fit_circle.',
				stroke_color : "'.$this->stroke_color.'",
				stroke_opacity : '.$this->stroke_opacity.',
				stroke_weight : '.$this->stroke_weight.',
				fill_color : "'.$this->fill_color.'",
				fill_opacity : '.$this->fill_opacity.',
				radius : '.$this->radius.',
				zoom_control : '.$this->zoom_control.',
				pan_control : '.$this->pan_control.',
				map_type_control : '.$this->map_type_control.',
				scale_control : '.$this->scale_control.',
				street_view_control : '.$this->street_view_control.'
			});
		</script>';
	
		//return the gmaps
		return $this->return_data;
	}

	// ----------------------------------------------------------------------------------

	/**
	 * geolocation
	 * 
	 * @return unknown_type
	 */
	function geolocation()
	{
		//call the init function to init some default values
		$this->_init();
		
		//set the class
		$this->address = $this->EE->TMPL->fetch_param('address') != '' ? $this->_build_js_array($this->EE->TMPL->fetch_param('address')) : '' ;
		$this->map_type = $this->EE->TMPL->fetch_param('map_type') != '' ? $this->EE->TMPL->fetch_param('map_type') : 'roadmap' ;
		$this->width = $this->EE->TMPL->fetch_param('width') != '' ? $this->EE->TMPL->fetch_param('width') : 400 ;
		$this->height = $this->EE->TMPL->fetch_param('height') != '' ? $this->EE->TMPL->fetch_param('height') : 400 ;
		$this->marker = $this->EE->TMPL->fetch_param('marker') == 'no' ? 'false' : 'true' ;
		$this->marker_icon = $this->EE->TMPL->fetch_param('marker_icon') != '' ? $this->EE->TMPL->fetch_param('marker_icon') : '' ;
		$this->zoom_control = $this->EE->TMPL->fetch_param('zoom_control') == 'no' ? 'false' : 'true' ;
		$this->pan_control = $this->EE->TMPL->fetch_param('pan_control')  == 'no' ? 'false' : 'true' ;
		$this->map_type_control = $this->EE->TMPL->fetch_param('map_type_control') == 'no' ? 'false' : 'true' ;
		$this->scale_control = $this->EE->TMPL->fetch_param('scale_control')  == 'no' ? 'false' : 'true' ;
		$this->street_view_control = $this->EE->TMPL->fetch_param('street_view_control') == 'no' ? 'false' : 'true' ;

		//return a div
		$this->return_data .= '<div id="'.$this->div_id.'" class="'.$this->div_class.'"></div>';
		
		//return the js 
		$this->return_data .= '<script type="text/javascript">
			EE_GMAPS.setGeolocation({
				selector : "'.$this->selector.'",
				address : "'.$this->address.'",
				map_type : "'.$this->map_type.'",
				width : '.$this->width.',
				height : '.$this->height.',
				marker : '.$this->marker.',
				marker_icon : "'.$this->marker_icon.'",
				zoom_control : '.$this->zoom_control.',
				pan_control : '.$this->pan_control.',
				map_type_control : '.$this->map_type_control.',
				scale_control : '.$this->scale_control.',
				street_view_control : '.$this->street_view_control.'
			});
		</script>';
	
		//return the gmaps
		return $this->return_data;
	}

	// ----------------------------------------------------------------------------------
	
	//add call timer
	private function _add_call_timer()
	{
		//set the time of calling inthe session
		if($this->EE->session->userdata('forms_js_validation_call_times') == '')
		{
			$this->EE->session->userdata['forms_js_validation_call_times'] = 1;
		}
		else
		{
			$this->EE->session->userdata['forms_js_validation_call_times'] = $this->EE->session->userdata('forms_js_validation_call_times') + 1;
		}
		
		//and if the add_base_files="yes" than set this to 1 to avoid problems when the first is not loaded
		if($this->add_base_files)
		{
			$this->caller_id = 1;
		}
		else
		{
			$this->caller_id = $this->EE->session->userdata('forms_js_validation_call_times');	
		}	
	}

	// ----------------------------------------------------------------------------------

	//build js array
	private function _build_js_array($addresses) 
	{
		$addresses = explode('|',$addresses);

		$_addresses = '[]';

		if(!empty($addresses))
		{
			$_addresses = '';
			
			foreach($addresses as $key => $address)
			{
				if($key == 0)
				{
					$_addresses .= '[';
				}
				
				if(count($addresses) == ($key +1 ))
				{
					$_addresses .= '"'.$address.'"';
				}
				else
				{
					$_addresses .= '"'.$address.'",';
				}
				
				if(count($addresses) == ($key +1 ))
				{
					$_addresses .= ']';
				}
			}
		}
		return $_addresses;
	}
	
	// ----------------------------------------------------------------------------------

	//compress js
	private function _compress_js($files)
	{
		require 'Minifier.php';

		$out = $this->theme_path . 'js/gmaps.min.js';
		$script = '';
		foreach($files as $file)
		{
			$script .= Minifier::minify(file_get_contents($file))."\n";
		}

		file_put_contents($out, $script);	
	}
	
	// ----------------------------------------------------------------------------------
	
	//  Plugin Usage
	// ----------------------------------------------------------------------------------

	// This function describes how the plugin is used.
	//  Make sure and use output buffering

	function usage()
	{
		ob_start();
		?>
		
		This plugin will bring some basic and advanced Google Maps technique to ExpressionEngine as plugin. 
	

		=============================
		The Tag(s)
		=============================

        {exp:gmaps:geocoding}
        {exp:gmaps:geolocation}
        {exp:gmaps:route}
        {exp:gmaps:polygon}
        {exp:gmaps:circle}
				
		
		==============
		EXAMPLE GEOCODING
		==============

		{exp:gmaps:geocoding 
			address="heerde"
		}

		==============
		TAG PARAMETERS
		==============

		div_id="" [OPTIONAL]
		the div ID name. Default is ee_gmap.

		div_class="" [OPTIONAL]
		the div CLass name.

		map_type="" [OPTIONAL]
		the map type. Default to hybrid.
		values : Hybrid, Roadmap, Satellite or Terrain

		zoom_control="" [OPTIONAL]
		place the zoom control on the map. Default to yes
		values: yes or no.

		pan_control="" [OPTIONAL]
		place the pan control on the map. Default to yes
		values: yes or no.

		map_type_control="" [OPTIONAL]
		place the map type control on the map. Default to yes
		values: yes or no.

		scale_control="" [OPTIONAL]
		place the scale control on the map. Default to yes
		values: yes or no.

		street_view_control="" [OPTIONAL]
		place the street view control on the map. Default to yes
		values: yes or no.
		
		width="" [OPTIONAL]
		the width of the gmap. Default to 400px.

		height="" [OPTIONAL]
		the height of the map. Default to 400px.

		address="" [REQUIRED]
		A address name, can also a name like 'reichenbach im vogtland or amsterdam or america'.

		zoom="" [OPTIONAL]
		the zoom level. Default to 15.

		marker="" [OPTIONAL]
		set the markers. Default to yes.
		values: yes or no.

		marker_icon="" [OPTIONAL]
		set icon for a the marker.

		marker_html="" [OPTIONAL]
		the html for inside the infowindow that popups on click.
		e.g: <h1>Yeah</h1>

		custom_marker_html="" [OPTIONAL]
		a custom marker structure html, replacement for the whole infowindow html.
		e.g:<div class='overlay'>this is the title of the window<div class='overlay_arrow above'></div></div>

		custom_marker_vertical_align="" [OPTIONAL]
		set the marker vertical alignment. Default to top.
		values: top, middle or bottom

		custom_marker_horizontal_align="" [OPTIONAL]
		set the marker horizontal alignment. Default to center.
		values: left, center or right.

		static="" [OPTIONAL]
		is this a static map. Default to no. (This will create a image of the map)
		values: yes or no.



		==============
		EXAMPLE GEOLOCATION
		==============

		{exp:gmaps:geolocation}
		
		==============
		TAG PARAMETERS
		==============

		div_id="" [OPTIONAL]
		the div ID name. Default is ee_gmap.

		div_class="" [OPTIONAL]
		the div CLass name.

		zoom_control="" [OPTIONAL]
		place the zoom control on the map. Default to yes
		values: yes or no.

		pan_control="" [OPTIONAL]
		place the pan control on the map. Default to yes
		values: yes or no.

		map_type_control="" [OPTIONAL]
		place the map type control on the map. Default to yes
		values: yes or no.

		scale_control="" [OPTIONAL]
		place the scale control on the map. Default to yes
		values: yes or no.

		street_view_control="" [OPTIONAL]
		place the street view control on the map. Default to yes
		values: yes or no.

		map_type="" [OPTIONAL]
		the map type. Default to hybrid.
		values : Hybrid, Roadmap, Satellite or Terrain

		width="" [OPTIONAL]
		the width of the gmap. Default to 400px.

		height="" [OPTIONAL]
		the height of the map. Default to 400px.

		marker="" [OPTIONAL]
		set the markers. Default to yes.
		values: yes or no.

		marker_icon="" [OPTIONAL]
		set icon for a the marker.


		==============
		EXAMPLE ROUTE
		==============

		{exp:gmaps:route	
			from_address="heerde"
			to_address="apeldoorn"
			stops="epe nederland|klarenbeek"
		}

		==============
		TAG PARAMETERS
		==============

		div_id="" [OPTIONAL]
		the div ID name. Default is ee_gmap.

		div_class="" [OPTIONAL]
		the div CLass name.

		zoom_control="" [OPTIONAL]
		place the zoom control on the map. Default to yes
		values: yes or no.

		pan_control="" [OPTIONAL]
		place the pan control on the map. Default to yes
		values: yes or no.

		map_type_control="" [OPTIONAL]
		place the map type control on the map. Default to yes
		values: yes or no.

		scale_control="" [OPTIONAL]
		place the scale control on the map. Default to yes
		values: yes or no.

		street_view_control="" [OPTIONAL]
		place the street view control on the map. Default to yes
		values: yes or no.	

		map_type="" [OPTIONAL]
		the map type. Default to hybrid.
		values : Hybrid, Roadmap, Satellite or Terrain

		width="" [OPTIONAL]
		the width of the gmap. Default to 400px.

		height="" [OPTIONAL]
		the height of the map. Default to 400px.

		marker="" [OPTIONAL]
		set the markers. Default to yes.
		values: yes or no.

		marker_icon="" [OPTIONAL]
		set icon for a the marker.

		from_address="" [REQUIRED]
		A from address name, can also a name like 'reichenbach im vogtland or amsterdam or america'.

		to_address="" [REQUIRED]
		A to address name, can also a name like 'reichenbach im vogtland or amsterdam or america'.
		
		stops="" [OPTIONAL]
		stops between the from and to point. addresses seperated with a pipline '|'

		travel_mode="" [OPTIONAL]
		set the travel mode. Default to driving
		values: bicycling, driving or walking

		stroke_color="" [OPTIONAL]
		set the stroke color. Default to #131540

		stroke_opacity="" [OPTIONAL]
		set the stroke opacity. Default to 0.6

		stroke_weight="" [OPTIONAL]
		set the stroke weight. Default to 6

		fill_color="" [OPTIONAL]
		set the fill color. Default to #131540

		

		==============
		EXAMPLE POLYGON
		==============

		{exp:gmaps:polygon 
			address="den-helder|zwolle|deventer|gorssel|arnhem|klarenbeek|apeldoorn"
		}

		==============
		TAG PARAMETERS
		==============

		div_id="" [OPTIONAL]
		the div ID name. Default is ee_gmap.

		div_class="" [OPTIONAL]
		the div CLass name.

		zoom_control="" [OPTIONAL]
		place the zoom control on the map. Default to yes
		values: yes or no.

		pan_control="" [OPTIONAL]
		place the pan control on the map. Default to yes
		values: yes or no.

		map_type_control="" [OPTIONAL]
		place the map type control on the map. Default to yes
		values: yes or no.

		scale_control="" [OPTIONAL]
		place the scale control on the map. Default to yes
		values: yes or no.

		street_view_control="" [OPTIONAL]
		place the street view control on the map. Default to yes
		values: yes or no.

		map_type="" [OPTIONAL]
		the map type. Default to hybrid.
		values : Hybrid, Roadmap, Satellite or Terrain

		width="" [OPTIONAL]
		the width of the gmap. Default to 400px.

		height="" [OPTIONAL]
		the height of the map. Default to 400px.

		marker="" [OPTIONAL]
		set the markers. Default to yes.
		values: yes or no.

		marker_icon="" [OPTIONAL]
		set icon for a the marker.

		address="" [REQUIRED]
		multiple addresses, eg. addresses seperated with a pipline '|'

		stroke_color="" [OPTIONAL]
		set the stroke color. Default to #131540

		stroke_opacity="" [OPTIONAL]
		set the stroke opacity. Default to 0.6

		stroke_weight="" [OPTIONAL]
		set the stroke weight. Default to 6

		fill_color="" [OPTIONAL]
		set the fill color. Default to #131540

		static="" [OPTIONAL]
		is this a static map. Default to no. (This will create a image of the map)
		values: yes or no.


		==============
		EXAMPLE CIRCLE
		==============

		{exp:gmaps:circle 
			address="apeldoorn"
			radius="5000"
		}
		
		==============
		TAG PARAMETERS
		==============

		div_id="" [OPTIONAL]
		the div ID name. Default is ee_gmap.

		div_class="" [OPTIONAL]
		the div CLass name.

		zoom_control="" [OPTIONAL]
		place the zoom control on the map. Default to yes
		values: yes or no.

		pan_control="" [OPTIONAL]
		place the pan control on the map. Default to yes
		values: yes or no.

		map_type_control="" [OPTIONAL]
		place the map type control on the map. Default to yes
		values: yes or no.

		scale_control="" [OPTIONAL]
		place the scale control on the map. Default to yes
		values: yes or no.

		street_view_control="" [OPTIONAL]
		place the street view control on the map. Default to yes
		values: yes or no.

		map_type="" [OPTIONAL]
		the map type. Default to hybrid.
		values : Hybrid, Roadmap, Satellite or Terrain

		width="" [OPTIONAL]
		the width of the gmap. Default to 400px.

		height="" [OPTIONAL]
		the height of the map. Default to 400px.

		marker="" [OPTIONAL]
		set the markers. Default to yes.
		values: yes or no.

		marker_icon="" [OPTIONAL]
		set icon for a the marker.

		address="" [REQUIRED]
		A address name, can also a name like 'reichenbach im vogtland or amsterdam or america'.

		zoom="" [OPTIONAL]
		the zoom level. Default to 15.

		fit_circle="" [OPTIONAL]
		fit the circle on screen. Default to yes.

		stroke_color="" [OPTIONAL]
		set the stroke color. Default to #131540

		stroke_opacity="" [OPTIONAL]
		set the stroke opacity. Default to 0.6

		stroke_weight="" [OPTIONAL]
		set the stroke weight. Default to 6

		fill_color="" [OPTIONAL]
		set the fill color. Default to #131540

		fill_color="" [OPTIONAL]
		set the fill color. Default to #131540

		radius="" [OPTIONAL]
		set the radius. Default to 1000 ft


		<?php
		$buffer = ob_get_contents();

		ob_end_clean();

		return $buffer;
	}
	// END
}