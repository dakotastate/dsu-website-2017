<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Focus Lab, LLC Master Config
 *
 * This is the master config file for our ExpressionEngine sites
 * The settings will contain database credentials and numerous "config overrides"
 * used throughout the site. This file is used as first point of configuration
 * but there are environment-specific files as well. The idea is that the environment
 * config files contain config overrides that are specific to a single environment.
 *
 * Some config settings are used in multiple (but not all) environments. You will
 * see the use of conditionals around the ENV constant in this file. This constant is
 * defined in ./config/config.env.php
 *
 * All config files are stored in the ./config/ directory and this master file is "required"
 * in system/expressionengine/config/config.php and system/expressionengine/config/database.php
 *
 * require $_SERVER['DOCUMENT_ROOT'] . '/../config/config.master.php';
 *
 * This config setup is a combination of inspiration from Matt Weinberg and Leevi Graham
 * @link       http://eeinsider.com/articles/multi-server-setup-for-ee-2/
 * @link       http://ee-garage.com/nsm-config-bootstrap
 *
 * @package    Focus Lab Master Config
 * @version    1.1.1
 * @author     Focus Lab, LLC <dev@focuslabllc.com>
 * @see        https://github.com/focuslabllc/ee-master-config
 */


// Require our environment declatation file if it hasn't
// already been loaded in index.php or admin.php
if ( ! defined('ENV'))
{
	require $_SERVER['DOCUMENT_ROOT'].'/../nimbus-local/config/config.env.php';
}


// Setup our initial arrays
$env_db = $env_config = $env_global = $master_global = array();

// Utility Variables
$__segments = explode('/',parse_url($_SERVER['REQUEST_URI'],PHP_URL_PATH));


/**
 * Database override magic
 *
 * If this equates to TRUE then we're in the database.php file
 * We don't want these settings bothered with in our config.php file
 */
if (isset($db['expressionengine']))
{

	/**
	 * Use the credentials found in config.env.php
	 */
	$env_db['database'] = CR_DB_NAME;
	$env_db['username'] = CR_DB_USER;
	$env_db['password'] = CR_DB_PASS;
	$env_db['hostname'] = CR_DB_HOST;

	// Dynamically set the cache path (Shouldn't this be done by default? Who moves the cache path?)
	$env_db['cachedir'] = APPPATH . 'cache/db_cache/';

	// Merge our database setting arrays
	$db['expressionengine'] = array_merge($db['expressionengine'], $env_db);

	// No need to have this variable accessible for the rest of the app
	unset($env_db);
}
// End if (isset($db['expressionengine'])) {}


/**
 * Config override magic
 *
 * If this equates to TRUE then we're in the config.php file
 * We don't want these settings bothered with in our database.php file
 */
if (isset($config))
{

	/**
	 * Dynamic path settings
	 *
	 * Make it easy to run the site in multiple environments and not have to switch up
	 * path settings in the database after each migration
	 * As inspired by Matt Weinberg: http://eeinsider.com/articles/multi-server-setup-for-ee-2/
	 */

	/* Sets the protocol based on the X-Forwarded-Proto in request header for load balanced situations. */
	$headers = apache_request_headers();
	foreach ($headers as $header => $value) {
			if ($header == 'X-Forwarded-Proto'){
				$protocol = $value.'://';
			}
	}

	/* Sets the protocol based on the server protocol in cases where the X-Forwarded-Proto is missing in request header. */
	if (!isset($protocol)){
		$protocol                          = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') ? 'https://' : 'http://';
	}

	$base_url                          = $protocol . $_SERVER['HTTP_HOST'];
	$base_path                         = rtrim($_SERVER['DOCUMENT_ROOT'],'/').'/';
	$system_folder                     = APPPATH . '../';
	$images_folder                     = 'images';
	$images_path                       = $base_path . 'assets/nimbus/' . $images_folder;
	$images_url                        = $base_url . '/assets/nimbus/' . $images_folder;
	$uploads_base_path                 = $base_path . 'assets/uploads/';
	$uploads_base_url                  = $base_url . '/assets/uploads/';

	$env_config['index_page']          = '';
	$env_config['site_index']          = '';
	$env_config['site_label']          = CR_SITE_NAME;
	$env_config['license_number']      = '1123-5813-2134-5589';
	$env_config['base_path']           = $base_path;
	$env_config['base_url']            = $base_url . '/';
	$env_config['site_url']            = $env_config['base_url'];
	$env_config['cp_url']              = $env_config['base_url'] . 'admin.php';
	$env_config['cp_theme']            = 'sassy_cp';
	$env_config['cache_path']          = $base_path . '../nimbus-local/cache/';
	$env_config['theme_folder_path']   = $base_path   . 'assets/nimbus/themes/';
	$env_config['theme_folder_url']    = $base_url    . '/assets/nimbus/themes/';
	$env_config['emoticon_path']       = $images_url  . '/smileys/';
	$env_config['emoticon_url']        = $images_url  . '/smileys/';
	$env_config['captcha_path']        = $images_path . '/captchas/';
	$env_config['captcha_url']         = $images_url  . '/captchas/';
	$env_config['avatar_path']         = $images_path . '/avatars/';
	$env_config['avatar_url']          = $images_url  . '/avatars/';
	$env_config['photo_path']          = $images_path . '/member_photos/';
	$env_config['photo_url']           = $images_url  . '/member_photos/';
	$env_config['sig_img_path']        = $images_path . '/signature_attachments/';
	$env_config['sig_img_url']         = $images_url  . '/signature_attachments/';
	$env_config['prv_msg_upload_path'] = $images_path . '/pm_attachments/';
	$env_config['third_party_path']    = $base_path . '../nimbus-local/third-party/';
	$env_config['path_third_themes']   = $base_path . 'assets/nimbus/third-party/';
	$env_config['url_third_themes']    = $base_url  . '/assets/nimbus/third-party/';


	/**
	 * Custom upload directory paths
	 *
	 * The array keys must match the ID from exp_upload_prefs
	 */
	$env_config['upload_preferences'] = array(
		1 => array(
			'name'        => 'General Uploads',
			'server_path' => $uploads_base_path . 'general/',
			'url'         => $uploads_base_url  . 'general/'
		),
		2 => array(
			'name'        => 'Hero Image Uploads',
			'server_path' => $uploads_base_path . 'hero-images/',
			'url'         => $uploads_base_url  . 'hero-images/'
		),
		3 => array(
			'name'        => 'News & Events Uploads',
			'server_path' => $uploads_base_path . 'news-events/',
			'url'         => $uploads_base_url  . 'news-events/'
		),
		4 => array(
			'name'        => 'Resources',
			'server_path' => $uploads_base_path . 'resources/',
			'url'         => $uploads_base_url  . 'resources/'
		),
		5 => array(
			'name'        => 'Directory',
			'server_path' => $uploads_base_path . 'directory/',
			'url'         => $uploads_base_url  . 'directory/'
		)
	);


	/**
	 * Auto-detect Windows environment and switch slashes for upload preferences (if set)
	 */
	if ( isset($env_config['upload_preferences']) && is_array($env_config['upload_preferences']) && strtoupper(substr(PHP_OS, 0, 3)) === 'WIN' )
	{
		foreach ($env_config['upload_preferences'] as $fp_id => $fp_data)
			$env_config['upload_preferences'][$fp_id]['server_path'] = str_replace('/', '\\', $fp_data['server_path']);
	}


	/**
	 * Template settings
	 *
	 * Working locally we want to reference our template files.
	 * In staging and production we do not use flat files (for ever-so-slightly better performance)
	 * This approach requires that we synchronize templates after each deployment of template changes
	 *
	 * For the distributed Focus Lab, LLC Master Config file this is commented out
	 * You can enable this "feature" by uncommenting the second 'save_tmpl_files' line
	 */
	$env_config['save_tmpl_files']           = 'y';
	$env_config['tmpl_file_basepath']        = $base_path . '../nimbus-local/templates/ee';
	$env_config['hidden_template_indicator'] = '_';


	/**
	 * Snippets
	 */
	$env_config['snippet_file_basepath']     = $base_path . '../nimbus-local/templates/snippets/';


	/**
	 * Debugging settings
	 *
	 * These settings are helpful to have in one place
	 * for debugging purposes
	 */
	$env_config['is_system_on']         = 'y';
	$env_config['allow_extensions']     = 'y';
	$env_config['email_debug']          = (ENV_DEBUG) ? 'y' : 'n' ;
	// If we're not in production show the profile on the front-end but not in the CP
	$env_config['show_profiler']        = ( ! ENV_DEBUG OR (isset($_SERVER['SCRIPT_NAME']) && $_SERVER['SCRIPT_NAME'] == '/admin.php')) ? 'n' : 'y' ;
	// Show template debugging if we're not in production
	$env_config['template_debugging']   = (ENV_DEBUG) ? 'y' : 'n' ;
	/**
	 * Set debug to '2' if we're in dev mode, otherwise just '1'
	 *
	 * 0: no PHP/SQL errors shown
	 * 1: Errors shown to Super Admins
	 * 2: Errors shown to everyone
	 */
	$env_config['debug']                = (ENV_DEBUG) ? '2' : '1' ;

	/**
	 * Set Environmental New Relic App Name
	 */
	$env_config['newrelic_app_name'] = ENV_FULL;


	/**
	 * Override Profiler & Template Debug for AJAX Pages
	 */
	if ( isset($__segments[1]) && $__segments[1] == 'dynamic' )
	{
		$env_config['show_profiler']         = 'n';
		$env_config['template_debugging']    = 'n';
	}
	if ( isset($__segments[1]) && $__segments[1] == 'sitemap.xml' )
	{
		$env_config['show_profiler']         = 'n';
		$env_config['template_debugging']    = 'n';
	}


	/**
	 * Tracking & Performance settings
	 *
	 * These settings may impact what happens on certain page loads
	 * and turning them off could help with performance in general
	 */
	$env_config['disable_all_tracking']        = 'y'; // If set to 'y' some of the below settings are disregarded
	$env_config['enable_sql_caching']          = 'n';
	$env_config['disable_tag_caching']         = 'n';
	$env_config['enable_online_user_tracking'] = 'n';
	$env_config['dynamic_tracking_disabling']  = '500';
	$env_config['enable_hit_tracking']         = 'n';
	$env_config['enable_entry_view_tracking']  = 'n';
	$env_config['log_referrers']               = 'n';
	$env_config['gzip_output']                 = (ENV_DEBUG) ? 'n': 'y'; // Set to 'n' if your host is EngineHosting


	/**
	 * 3rd Party Add-on config items as needed
	 */

	// Freemember
	$env_config['encryption_key']              = 'sa@dkhvds3lkjpoidvjvkajshuasdhvi!asu-vhkasjdvksh';

	// Stash
	$env_config['stash_cache_on']              = (ENV_DEBUG)? 'no': 'yes';
	$env_config['stash_cache_expire_default']  = '60';
	$env_config['stash_cache_expire_fast']     = '5';
	$env_config['stash_cache_flush']           = FALSE;
	$env_config['stash_file_basepath']         = $base_path . '../nimbus-local/templates/stash/';
	$env_config['stash_file_sync']             = (ENV_DEBUG)? TRUE: FALSE; // set to FALSE for production
	$env_config['stash_cookie']                = 'stashid'; // the stash cookie name
	$env_config['stash_cookie_expire']         = 0; // seconds - 0 means expire at end of session
	$env_config['stash_default_scope']         = 'local'; // default variable scope if not specified
	$env_config['stash_limit_bots']            = TRUE; // stop database writes by bots to reduce load on busy sites
	$env_config['stash_bots']                  = array('bot', 'crawl', 'spider', 'archive', 'search', 'java', 'yahoo', 'teoma');

	// Buster
	$env_config['buster_enabled']           = TRUE; // (ENV_DEBUG)? FALSE : TRUE;

	// Resource Router
	$env_config['resource_router'] = array(
		'sitemap.xml' => 'home/sitemap.xml',

		// Events
		// ----------------
		// Send all of the different events URLs to the same template
		// to handle it all in the same place.
		//
		//'events' => 'home/events',
		//'events/:pagination' => 'home/events',
		//'events/category/:any' => 'home/events',
		//'events/date/:year/:month' => 'home/events',
		//'events/date/:year/:month/:day' => 'home/events',

		// Landing Pages!!!
		// ----------------
		// This tests the first segment, and if it's a valid url_title in the landing_pages
		// group, then we route to the associated template. Also, we set a global variable
		// identifying this as a landing page.
		':url_title' => function($router,$wildcard) {
			if ( $wildcard->isValidUrlTitle( array('channel' => 'landing_pages') ) )
			{
				// Set landing page global variable
				$router->setGlobal('cr:landing_page','1');

				// Query for the associated template
				$tmpl_q = ee()->db->query('SELECT tg.group_name AS `group`, t.template_name AS `name` FROM exp_channel_titles AS ct, exp_structure_listings AS sl, exp_templates AS t, exp_template_groups AS tg WHERE ct.url_title = \'?\' && sl.entry_id = ct.entry_id && t.template_id = sl.template_id && tg.group_id = t.group_id', array($wildcard));

				if ( $tmpl_q->num_rows() > 0 )
				{
					// If we find the template, display it
					$tmpl = $q->row();
					$router->setTemplate("{$tmpl->group}/{$tmpl->name}");
				} else {
					// Otherwise output a 404
					$router->set404();
				}
			}
		}
	);


	/**
	 * Other system settings
	 */

	// Member Settings
	$env_config['profile_trigger']          = rand(0,time()); // randomize the member profile trigger word

	// Miscellany
	$env_config['new_version_check']        = 'n'; // no slowing my CP homepage down with this
	$env_config['daylight_savings']         = ((bool) date('I')) ? 'y' : 'n'; // Autodetect DST
	$env_config['use_category_name']        = 'y';
	$env_config['reserved_category_word']   = 'category';
	$env_config['word_separator']           = 'dash'; // dash|underscore
	$env_config['redirect_method']          = 'location';
	//$env_config['mail_protocol']            = 'smtp';
	//$env_config['smtp_server']              = 'dsu-smtp-01.dsu.local';
	//$env_config['smtp_port']                = '25';
	//$env_config['smtp_password']            = '43wl7nves773';
	//$env_config['smtp_username']            = 'postmaster@mailgun.clickrain.net';
	//$env_config['webmaster_email']          = CR_DEV_EMAIL;
	// These two properties required to allow structure to redirect on login and on posting, respectively.
	$env_config['require_ip_for_login']     = 'n';
	$env_config['require_ip_for_posting']   = 'n';


	/**
	 * Setup our template-level global variables
	 *
	 * As inspired by NSM Bootstrap Config
	 * @see http://ee-garage.com/nsm-config-bootstrap
	 */
	global $assign_to_config;
	if( ! isset($assign_to_config['global_vars']))
	{
		$assign_to_config['global_vars'] = array();
	}

	// Start our array with environment variables. This gives us {global:env} and {global:env_full} tags for our templates.
	$master_global = array(
		'global:env'                        => ENV,
		'global:env_full'                   => ENV_FULL,
		'global:env_domain'                 => defined('CR_REQ_HOST')? CR_REQ_HOST: $_SERVER['HTTP_HOST'],
		'global:stash_replace'              => ENV_DEBUG ? 'yes' : 'no',
		'global:stash_save'                 => ENV_DEBUG ? 'no' : 'yes',
		'global:stash_cache_on'             => $env_config['stash_cache_on'],
		'global:stash_cache_expire_default' => $env_config['stash_cache_expire_default'],
		'global:stash_cache_expire_fast'    => $env_config['stash_cache_expire_fast'],
		'global:stash_cache_flush'          => ($env_config['stash_cache_flush'])? 'true': 'false',
	);



	/*
	 * ---------------------------------------------------------------------------------------------
	 *  GA LOCAL - Read Google Analytics / AdWords metrics into global vars. (Requires ga-local.js)
	 * ---------------------------------------------------------------------------------------------
	 */
	// Define the CRA & GA metrics we want to make global vars
	$__cra_metrics = array('crt_source','crt_campaign','crt_medium','crt_creative','crt_keyword','crt_adposition');
	$__ga_metrics  = array('referrer');
	$__all_metrics = array_merge( $__cra_metrics, $__ga_metrics );

	// Read CRA & GA Metric Data
	$__all_metric_data = isset( $_COOKIE['cr_ga_data'] )? json_decode($_COOKIE['cr_ga_data'],TRUE): array();

	// Override CRA Metric Data
	// -- Since GA data requires JS interaction with the Google Analytics API, it requires
	// -- at least one page load. We have no such limitation on custom tags, so we can override
	// -- cookied CRA data with anything that is present on the *current* page load.
	foreach ( $__cra_metrics as $key )
	{
		if ( isset( $_REQUEST[$key] ) )
		{
			$__all_metric_data[$key] = $_REQUEST[$key];
		}
	}

	// Set CRA Globals
	$master_global['cr:analytics:encoded'] = base64_encode(serialize($__all_metric_data));
	foreach ( $__all_metrics as $key )
	{
		$master_global['cr:analytics:' . $key] = ( isset($__all_metric_data[$key]) )? $__all_metric_data[$key]: '';
	}



	/**
	 * Merge arrays to form final datasets
	 *
	 * We've created our base config and global key->value stores
	 * We've also included the environment-specific arrays now
	 * Here we'll merge the arrays to create our final array dataset which
	 * respects "most recent data" first if any keys are duplicated
	 *
	 * This is how our environment settings are "king" over any defaults
	 */
	$assign_to_config['global_vars'] = array_merge($assign_to_config['global_vars'], $master_global, $env_global); // global var arrays
	$config = array_merge($config, $env_config); // config setting arrays


}
// End if (isset($config)) {}


/* End of file config.master.php */
/* Location: ./config/config.master.php */
