"use strict";

var fs = require('fs');

var nav = fs.readFileSync('primary-navigation.txt', 'utf-8');

var navs = nav.split('\n').filter(function(item) { return item.trim() != ''; });
var navs2 = navs.map(function(item) {
	var depth = 1;
	var url = '#firstlevel';
	if (item.indexOf('\t') > -1) {
		depth = 2;
		url = '#secondlevel';
	}
	if (item.indexOf('\t\t') > -1) {
		depth = 3;
		url = '#thirdlevel';
	}
	if (item.indexOf('\t\t\t') > -1) {
		depth = 4;
		url = '#fourthlevel';
	}
	item = item.trim();
	return {
		depth: depth,
		text: item,
		url: url
	};
});

var lastItem = null;

console.log('  <ul>');

navs2.forEach(function(item) {
	var prefix = '';
	if (lastItem && lastItem.depth < item.depth) {
		prefix = '';
		for (var i = 0; i < lastItem.depth; i++) {
			prefix += '    ';
		}
		prefix += '  ';
		console.log(prefix + '<ul>');
	}
	if (lastItem && lastItem.depth > item.depth) {
		prefix = '';
		for (var i = 0; i < lastItem.depth - 1; i++) {
			prefix += '    ';
		}
		console.log(prefix + '  </ul>\n' + prefix + '</li>');
	}

	prefix = '';
	for (var i = 0; i < item.depth; i++) {
		prefix += '    ';
	}

	console.log(prefix + '<li><a href="' + item.url + '">' + item.text + '</a>');

	lastItem = item;
});

console.log('  </ul>');

//console.log(navs2);
