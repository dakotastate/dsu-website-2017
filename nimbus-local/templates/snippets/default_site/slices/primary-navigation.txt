About DSU
	Why DSU?
	Virtual Tour
	Maps and Directions
	History
	Accreditation
	President's Office
		Support Staff
	Faculty & Staff Directory
		David Borofsky
		David Overby
		(Additional Faculty Members)
	Foundation
		Board of Trustees
		Foundation Staff
	Consumer Information
Admissions
	Apply Now
		Undergraduate Application
		Non-Degree Application
	Visit DSU
	Financial Aid & Tuition
		Scholarships
		Tuition and Fees
	Get Free Stuff
	Request More Info
	Admissions Requirements
		Baccalaureate Degree Program
		Associate Degree Programs
		Admission on Probation
		Transfer Students
		International Students
		Veteran Affairs
	Meet Our Admissions Team
	Admissions FAQs
	New Student Registration (Hidden)
Academics
	Degrees and Programs
		Bachelor's Degrees
			Accounting (BBA)
			Biology Education (BSE)
			Biology for Information Systems (BS)
			Business Education (BSE)
			Business Technology (BBA)
			Computer Education (BSE)
			Computer Game Design (BS)
			Computer Science (BS)
			Cyber Operations (BS)
			Digital Arts and Design: Audio Production (BS)
			Digital Arts and Design: Computer Graphics (BS)
			Digital Arts and Design: Digital Storytelling (BS)
			Digital Arts and Design: Production Animation (BS)
			Digital Arts and Design: Web Design and Production (BS)
			Elementary Education/Special Education (BSE)
			Elementary Education (BSE)
			English Education (BSE)
			English for New Media (BS)
			Exercise Science (BS)
			Finance (BBA)
			General Studies (BGS)
			Health Information Administration (BS)
			Information Systems (BS)
			Management (BBA)
			Marketing (BBA)
			Mathematics Education (BSE)
			Mathematics for Information Systems (BS)
			Network and Security Administration (BS)
			Physical Education (BSE)
			Physical Science (BS)
			Professional Accountancy (BS)
			Professional and Technical Communications (BS)
			Respiratory Care (BS)
		Associate's Degrees
			Business Management (AS)
			General Studies (AA)
			Health Information Technology (AS)
			Network and System Administration (AS)
			Respiratory Care (AS)
		Certificates
			English for New Media Certificate
			Health Care Coding Certificate
			High-Performance Computing Certificate
			IS Management Digital Photography Certificate
			IS Management Information Technology Management Certificate
			IS Management Multimedia Certificate
			IS Management Multimedia Design and Production Certificate
			IS Management Network and Telecommunications Administration Certificate
			IS Management Object Oriented Programming Certificate
			IS Management Technology Database Management Systems Certificate
			IS Management Web Application Development Certificate
			IS Management Website Administration Certificate
			IS Management Website Design and Development Certificate
			Information Technology Entrepreneurship Certificate
			Online Secondary Education Certificate
			Professional and Technical Communication Certificate
			Teacher Education - Certification Only Program
		Minors
			Minors
	Distance & Online Education & Other text that will make this item wrap
		Online Programs
		Tuition and Payment
		Fast Track Courses
		University Center Programs
		Distance Education Faculty and Staff
	Academic Opportunities
		Advanced Informatics Research Lab (AIRL)
		Center for Theoretical Underground Physics and Related Areas (CETUP)
		Center for the Protection of the Financial Infrastructure (CPFI)
		Center for Security Technology (CST)
		Information Assurance (IA) Programs
		Faculty Research Initiative
		Research Experiences for Undergraduates (REU)
	Tablet Program
	Library
	Bookstore
	ADA Accommodations
Grad Students
	Grad Admissions
		Graduate Application
		Tuition and Fees
		Graduate Assistanceships
		International Admissions
		Request More Information
	Doctor of Science in Information Systems
		Course Summary
		Faculty
		Timelines and Guidelines
		(Additional as needed)
	MBA
		Course Summary
		Faculty
		Timelines and Guidelines
		(Additional as needed)
	MSA
		Course Summary
		Faculty
		Timelines and Guidelines
		(Additional as needed)
	MSACS
		Course Summary
		Faculty
		Timelines and Guidelines
		(Additional as needed)
	MSET
		Course Summary
		Faculty
		Timelines and Guidelines
		(Additional as needed)
	MSHI
		Course Summary
		Faculty
		Timelines and Guidelines
		(Additional as needed)
	MSIA
		Course Summary
		Faculty
		Timelines and Guidelines
		(Additional as needed)
	MSIS
		Course Summary
		Faculty
		Timelines and Guidelines
		(Additional as needed)
	Business Analytics Certificate
	Ethical Hacking Certificate
Student Life
	Residence Life
		Residence Halls
		FYRE (First-Year Residence Experience)
		What to Bring
		Housing FAQs
	Clubs and Organizations
		Art Club
		Cheer Dance Team
		Colleges Against Cancer
		Computer Club
		Delta Mu Delta
		DSU2
		Gaming Club
		English Club
		International Club
		Intervarsity Christian Fellowship
		Intramurals
		Kappa Sigma Iota
		KDSU
		Martial Arts Club
		PBL
		PE
		Respiratory Care Club
		SAB
		SDEA
		SIFE
		Student Association Senate
		WIST
	Dining Services
	Student Health
	Study Abroad Opportunities
		Tamagawa Program
		Exchange Programs
	Campus Security
	Diversity
	Disability Services
	Student Success Center
	Student Handbook
Alumni and Friends
	Give a Gift
	Update Your Information
	DSU License Plates
	Alumni Magazine
	Alumni Testimonials
		Rebecca Bessman '07
		[Other testimonials as needed]
	Official Transcripts
	Alumni Board
	Career Services
Athletics
